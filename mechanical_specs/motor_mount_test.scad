/**
 * 3D model test print for motor shaft clearance testing.
 *
 * All dimensions are in mm.
 */

// Increase resolution of curves.
$fn = 180;
$fa = 3;

/*
 * Printer constants.
 */
layer_height = 0.2;
nozzle_size = 0.4;
shrink_ratio = 0.004;

/*
 * Model constants/parameters.
 */
// Max structure height.
max_height = 30 * nozzle_size;
core_diameter = 8;
core_hole_diameter_start = 2.04;
core_hole_diameter_end = 2.10;
core_hole_diameter_switch_ratio = 1 / 3;
core_cutout_ball = [1.75, 4, 3];
container_core_spacing = 0.1;
container_tolerance_ratio = 0.03;
container_bottom_layers = 3;
container_wall_layers = 2;
container_inner_width = 12;
container_inner_length = 4 * 2.75;
container_recesses_hide_ratio = 1 / 100;
bridge_center_offset = 1;
bridge_center_diameter = 3;

// Precalc based on constants.
core_hole_height_start = max_height * core_hole_diameter_switch_ratio;
core_hole_height_end = max_height * (1 - core_hole_diameter_switch_ratio);
core_hole_diameter_start_print = core_hole_diameter_start * (1 + shrink_ratio);
core_hole_diameter_end_print = core_hole_diameter_end * (1 + shrink_ratio);

container_wall_size = nozzle_size * container_wall_layers;
container_bottom_size = layer_height * container_bottom_layers;
container_center_offset = container_core_spacing + core_diameter / 2;
container_inner_width_print = container_inner_width * (1 + container_tolerance_ratio + shrink_ratio);
container_inner_length_print = container_inner_length * (1 + container_tolerance_ratio + shrink_ratio);
container_inner_height = max_height - container_bottom_size;
container_inner_cube_size = [container_inner_length_print, container_inner_width_print, container_inner_height];
container_inner_bottom_cube_size = [container_inner_length_print, container_inner_width_print, container_inner_height / 2];
container_outer_width = container_inner_width_print + container_wall_size * 2;
container_outer_length = container_inner_length_print + container_wall_size * 2;
container_recesses_height_fix = container_inner_height / container_inner_width_print;
container_recesses_hide_offset = -(container_recesses_hide_ratio * (container_inner_height / 2));

core_cutout_diameter = core_diameter + container_core_spacing;
core_cutout_radius = core_cutout_diameter / 2;
core_cutout_point = [core_diameter / 2 + container_core_spacing, container_outer_width / 2, max_height];
core_cutout_point_local = [core_cutout_point.x / core_cutout_ball.x, core_cutout_point.y / core_cutout_ball.y, core_cutout_point.z / core_cutout_ball.z];
core_cutout_offset_local = sqrt(pow(core_cutout_radius, 2) - pow(core_cutout_point_local.x, 2) - pow(core_cutout_point_local.y, 2)) + core_cutout_point_local.z;
core_cutout_offset = core_cutout_offset_local * core_cutout_ball.z;

// Specification report.
echo(str("Core shaft start diameter: ", core_hole_diameter_start_print, "mm"));
echo(str("Core shaft end diameter: ", core_hole_diameter_end_print, "mm"));
echo(str("Container bottom thickness = ", container_bottom_size, "mm"));
echo(str("Container wall thickness = ", container_wall_size, "mm"));
echo(str("Container inner size: ", container_inner_length_print, "x", container_inner_width_print, "x", container_inner_height, "mm"));
echo(str("Container outer size: ", container_outer_length, "x", container_outer_width, "x", max_height, "mm"));
echo(str("Full model size: ", (container_outer_length + container_core_spacing) * 2 + core_diameter, "x", container_outer_width, "x", max_height, "mm"));

module core() {
  difference() {
    difference() {
      union() {
        translate([0, 0, max_height / 2]) {
          cylinder(h = max_height, d = core_diameter, center = true);
        }
        bridge();
        mirror([1, 0, 0]) bridge();
      }
      union() {
        translate([0, 0, core_hole_height_start / 2]) {
          cylinder(h = core_hole_height_start, d = core_hole_diameter_start_print, center = true);
        }
        translate([0, 0, core_hole_height_start + core_hole_height_end / 2]) {
          cylinder(h = core_hole_height_end, d = core_hole_diameter_end_print, center = true);
        }
      }
    }
    translate([0, 0, core_cutout_offset]) scale(core_cutout_ball) sphere(d = core_cutout_diameter);
  }
}

module container_corner_a() {
  translate([
    container_center_offset + container_wall_size / 2,
    container_inner_width_print / 2 + container_wall_size / 2,
    max_height / 2
  ]) {
    cylinder(h = max_height, d = container_wall_size, center = true);
  }
}

module container_corner_b() {
  mirror([0, 1, 0]) container_corner_a();
}

module container_corner_c() {
  translate([
    container_inner_length_print + container_center_offset + container_wall_size * (3 / 2),
    container_inner_width_print / 2 + container_wall_size / 2,
    max_height / 2
  ]) {
    cylinder(h = max_height, d = container_wall_size, center = true);
  }
}

module container_corner_d() {
  mirror([0, 1, 0]) container_corner_c();
}

module container() {
  union() {
    difference() {
      hull() {
        container_corner_a();
        container_corner_b();
        container_corner_c();
        container_corner_d();
      }
      translate([
        container_inner_length_print / 2 + container_center_offset + container_wall_size,
        0, container_bottom_size + container_inner_height / 2
      ]) {
        cube(container_inner_cube_size, center = true);
      }
    }
    difference() {
      translate([
        container_inner_length_print / 2 + container_center_offset + container_wall_size,
        0, container_bottom_size + container_inner_height / 4
      ]) {
        cube(container_inner_bottom_cube_size, center = true);
      }
      translate([
        container_inner_length_print / 2 + container_center_offset + container_wall_size,
        0, container_recesses_hide_offset + container_bottom_size + container_inner_height / 2
      ]) {
        scale([1, 1, container_recesses_height_fix]) rotate([0, 90, 0]) {
          cylinder(d = container_inner_width_print, h = container_inner_length_print, center = true);
        }
      }
    }
  }
}

module bridge() {
  hull() {
    container_corner_a();
    container_corner_b();
    translate([bridge_center_offset, 0, max_height / 2]) {
      cylinder(h = max_height, d = bridge_center_diameter, center = true);
    }
  }
}

module motor_mount() {
  union() {
    core();
    container();
    mirror([1, 0, 0]) container();
  }
}

if($preview) {
  //render(convexity = 10)
  motor_mount();
} else {
  motor_mount();
}
