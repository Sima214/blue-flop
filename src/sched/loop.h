#ifndef BLFL_LOOP
#define BLFL_LOOP

#include <stdint.h>

/**
 * Return zero to not be called again.
 */
typedef int (*tick_callback_f)(void* data);

/**
 * Enter main loop.
 */
void main_loop();

/**
 * Exit main loop.
 */
void trigger_exit();

/**
 * Returns non zero if all slots are filled.
 */
int register_periodic_callback(uint32_t period_ms, tick_callback_f fnc, void* userdata);

/**
 * Continue executing this when all interrupts end.
 */
int register_thread_callback();

/**
 * One time high precision callback.
 */
int register_alarm_callback();

#endif /*BLFL_LOOP*/