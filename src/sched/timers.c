#include "timers.h"

#include <cm3ex.h>
#include <macros.h>
#include <power.h>
#include <stats.h>

#include <cm3/systick.h>
#include <cm3/vector.h>
#include <stm32/rtc.h>

#include <stdint.h>

#define TIMER_SYSTICK_INTERVAL 3330
#define TIMER_RTC_INTERVAL 336
#define CLOCK_PERIOD (1.0 / 72.0)

static uint32_t volatile timer_rtc_seconds;
static int32_t volatile timer_rtc_microsec;
// Will overflow at about 5 months, 13 days, 8 hours, 25 minutes and 22 seconds since boot.
static uint32_t volatile system_ticks;
static uint32_t volatile system_ticks_next_wakeup;
// RTC wraps around every 400 hours, 29 minutes, 52 seconds.

interrupt void sys_tick_handler() {
  system_ticks++;
  if(system_ticks_next_wakeup && system_ticks >= system_ticks_next_wakeup) {
    power_wakeup(1);
  }
  timer_rtc_microsec += TIMER_SYSTICK_INTERVAL;
  if(cold_branch(timer_rtc_microsec > 999999)) {
    timer_rtc_microsec -= 1000000;
    timer_rtc_seconds++;
    stats.mem.ram_usage = ((uintptr_t)&_stack) - ((uintptr_t)__builtin_frame_address(0));
  }
}

interrupt void rtc_alarm_isr() {
  rtc_clear_flag(RTC_ALR);
  // TODO: priority queue.
}

void timer_init(uint32_t unix_sec, int32_t u_sec) {
  timer_rtc_seconds = unix_sec;
  timer_rtc_microsec = u_sec;
  // Configure systick as the main time keeping device.
  systick_set_clocksource(STK_CSR_CLKSOURCE_AHB);
  systick_set_reload(((uint32_t)(TIMER_SYSTICK_INTERVAL / CLOCK_PERIOD)) - 1);
  // Start off!
  systick_clear();
  systick_interrupt_enable();
  systick_counter_enable();
  // Use RTC peripheral for extra task scheduling.
  rtc_auto_awake(RCC_HSE, 21);
  rtc_set_alarm_time(0);
  rtc_enable_alarm();
  rtc_clear_flag(RTC_ALR);
  nvic_enable_irq(NVIC_RTC_ALARM_IRQ);
  nvic_set_correct_priority(NVIC_RTC_ALARM_IRQ, 8);
  rtc_interrupt_enable(RTC_ALR);
}

int64_t timer_get_ms() {
  return ((int64_t)timer_rtc_seconds) * 1000 + timer_rtc_microsec / 1000;
}

uint32_t timer_get_ticks() {
  return system_ticks;
}

uint32_t timer_get_subticks() {
  return systick_get_value();
}

uint32_t timer_calc_sleep_until_ticks(uint32_t usec) {
  uint32_t required_ticks = usec / TIMER_SYSTICK_INTERVAL;
  return system_ticks + required_ticks;
}

void timer_sleep_until_ticks(uint32_t ticks) {
  system_ticks_next_wakeup = ticks;
  while(system_ticks < system_ticks_next_wakeup) {
    power_sleep();
  }
  system_ticks_next_wakeup = 0;
}

void timer_sleep_usec(uint32_t usec) {
  timer_sleep_until_ticks(timer_calc_sleep_until_ticks(usec));
}

void timer_sleep_msec(uint32_t msec) {
  timer_sleep_usec(msec * 1000);
}

void timer_sleep_sec(uint32_t sec) {
  timer_sleep_msec(sec * 1000);
}

/**
 * Based on newlib's gmtime_r implementation:
 * 
 * gmtime_r.c
 * Original Author: Adapted from tzcode maintained by Arthur David Olson.
 * Modifications:
 * - Changed to mktm_r and added __tzcalc_limits - 04/10/02, Jeff Johnston
 * - Fixed bug in mday computations - 08/12/04, Alex Mogilnikov <alx@intellectronika.ru>
 * - Fixed bug in __tzcalc_limits - 08/12/04, Alex Mogilnikov <alx@intellectronika.ru>
 * - Move code from _mktm_r() to gmtime_r() - 05/09/14, Freddie Chopin <freddie_chopin@op.pl>
 * - Fixed bug in calculations for dates after year 2069 or before year 1901. Ideas for
 *   solution taken from musl's __secs_to_tm() - 07/12/2014, Freddie Chopin
 *   <freddie_chopin@op.pl>
 * - Use faster algorithm from civil_from_days() by Howard Hinnant - 12/06/2014,
 * Freddie Chopin <freddie_chopin@op.pl>
 *
 * Converts the calendar time pointed to by tim_p into a broken-down time
 * expressed as local time. Returns a pointer to a structure containing the
 * broken-down time.
 */
#define SECSPERMIN 60L
#define MINSPERHOUR 60L
#define HOURSPERDAY 24L
#define SECSPERHOUR (SECSPERMIN * MINSPERHOUR)
#define SECSPERDAY (SECSPERHOUR * HOURSPERDAY)
#define DAYSPERWEEK 7
#define MONSPERYEAR 12

#define YEAR_BASE 1970
#define EPOCH_YEAR 1970
#define EPOCH_WDAY 4
#define EPOCH_YEARS_SINCE_LEAP 2
#define EPOCH_YEARS_SINCE_CENTURY 70
#define EPOCH_YEARS_SINCE_LEAP_CENTURY 370

#define isleap(y) ((((y) % 4) == 0 && ((y) % 100) != 0) || ((y) % 400) == 0)

/* Move epoch from 01.01.1970 to 01.03.0000 (yes, Year 0) - this is the first
 * day of a 400-year long "era", right after additional day of leap year.
 * This adjustment is required only for date calculation, so instead of
 * modifying time_t value (which would require 64-bit operations to work
 * correctly) it's enough to adjust the calculated number of days since epoch.
 */
#define EPOCH_ADJUSTMENT_DAYS 719468L
/* year to which the adjustment was made */
#define ADJUSTED_EPOCH_YEAR 0
/* 1st March of year 0 is Wednesday */
#define ADJUSTED_EPOCH_WDAY 3
/* there are 97 leap years in 400-year periods. ((400 - 97) * 365 + 97 * 366) */
#define DAYS_PER_ERA 146097L
/* there are 24 leap years in 100-year periods. ((100 - 24) * 365 + 24 * 366) */
#define DAYS_PER_CENTURY 36524L
/* there is one leap year every 4 years */
#define DAYS_PER_4_YEARS (3 * 365 + 366)
/* number of days in a non-leap year */
#define DAYS_PER_YEAR 365
/* number of days in January */
#define DAYS_IN_JANUARY 31
/* number of days in non-leap February */
#define DAYS_IN_FEBRUARY 28
/* number of years per era */
#define YEARS_PER_ERA 400

const char* DAY_NAMES[] = {
  "Sunday", "Monday", "Tuesday", "Wednesday",
  "Thursday", "Friday", "Saturday"};

const char* MONTH_NAMES[] = {
  "January", "February", "March",
  "April", "May", "June", "July",
  "August", "September", "October",
  "November", "December"};

human_time_t timer_get_human_time() {
  uint32_t snapshot_seconds = timer_rtc_seconds;
  int32_t snapshot_microsec = timer_rtc_microsec;
  human_time_t ret;

  // Compute ms from μs.
  ret.ms = snapshot_microsec / 1000;

  int days = snapshot_seconds / SECSPERDAY + EPOCH_ADJUSTMENT_DAYS;
  int rem = snapshot_seconds % SECSPERDAY;
  if(rem < 0) {
    rem += SECSPERDAY;
    --days;
  }

  // Compute hour, min, and sec.
  ret.hour = rem / SECSPERHOUR;
  rem %= SECSPERHOUR;
  ret.min = rem / SECSPERMIN;
  ret.sec = rem % SECSPERMIN;

  // Compute day of week.
  int weekday = (ADJUSTED_EPOCH_WDAY + days) % DAYSPERWEEK;
  if(weekday < 0) {
    weekday += DAYSPERWEEK;
  }
  ret.day_name = DAY_NAMES[weekday];

  /**
   * Compute year, month, day & day of year.
   * For description of this algorithm see:
   * http://howardhinnant.github.io/date_algorithms.html#civil_from_days
   */
  int era = (days >= 0 ? days : days - (DAYS_PER_ERA - 1)) / DAYS_PER_ERA;
  // [0, 146096]
  int eraday = days - era * DAYS_PER_ERA;
  // [0, 399]
  int erayear = (eraday - eraday / (DAYS_PER_4_YEARS - 1) + eraday / DAYS_PER_CENTURY - eraday / (DAYS_PER_ERA - 1)) / 365;
  // [0, 365]
  int yearday = eraday - (DAYS_PER_YEAR * erayear + erayear / 4 - erayear / 100);
  // [0, 11]
  int month = (5 * yearday + 2) / 153;
  // [1, 31]
  int day = yearday - (153 * month + 2) / 5 + 1;
  month += month < 10 ? 2 : -10;
  int year = ADJUSTED_EPOCH_YEAR + erayear + era * YEARS_PER_ERA + (month <= 1);

  ret.year = year - YEAR_BASE;
  ret.month_name = MONTH_NAMES[month];
  ret.mday = day;

  return ret;
}