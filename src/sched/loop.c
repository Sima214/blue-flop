#include "loop.h"

#include <com.h>
#include <macros.h>
#include <power.h>
#include <timers.h>

#include <stddef.h>
#include <stdint.h>

typedef struct {
  tick_callback_f callback;
  void* userdata;
  int period : 24;
  int taken : 1;
} sched_slot_t;

static const uint32_t MAIN_LOOP_DELAY = 100 * 1000;

static sched_slot_t SCHED_SLOTS[128];

static int volatile exit = 0;

void trigger_exit() {
  exit = 1;
}

int register_tick_callback(uint32_t period_ms, tick_callback_f fnc, void* userdata) {
  uint32_t loop_ticks = (period_ms * 1000) / MAIN_LOOP_DELAY;
  if(loop_ticks == 0) {
    loop_ticks = 1;
  }
  // Fill the first empty slot you find.
  for(size_t i = 0; i < (sizeof(SCHED_SLOTS) / sizeof(sched_slot_t)); i++) {
    if(!SCHED_SLOTS[i].taken) {
      SCHED_SLOTS[i].callback = fnc;
      SCHED_SLOTS[i].userdata = userdata;
      SCHED_SLOTS[i].period = loop_ticks;
      SCHED_SLOTS[i].taken = 1;
      return 0;
    }
  }
  return 1;
}

void main_loop() {
  // Handler mode.
  uint32_t next_wakeup = timer_calc_sleep_until_ticks(MAIN_LOOP_DELAY);
  // How many 100ms ticks have elapsed.
  uint32_t ticks = 0;
  while(!exit) {
    // Accurate delay.
    timer_sleep_until_ticks(next_wakeup);
    next_wakeup = timer_calc_sleep_until_ticks(MAIN_LOOP_DELAY);
    ticks++;
    // Run periodic routines.
    for(size_t i = 0; i < (sizeof(SCHED_SLOTS) / sizeof(sched_slot_t)); i++) {
      if(SCHED_SLOTS[i].taken && (ticks % SCHED_SLOTS[i].period) == 0) {
        int res = SCHED_SLOTS[i].callback(SCHED_SLOTS[i].userdata);
        if(!res) {
          SCHED_SLOTS[i].taken = 0;
        }
      }
    }
    //com_updt_flush();
  }
}