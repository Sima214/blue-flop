#ifndef BLFL_TIMERS
#define BLFL_TIMERS

#include <stdint.h>
#include <time.h>

typedef struct {
    /** Years since 1970 */
    uint16_t year;
    /** Current day of the month */
    uint8_t mday;
    /** Current hour of the day */
    uint8_t hour;
    /** Current minute of the hour */
    uint8_t min;
    /** Current second of the minute */
    uint8_t sec;
    /** Milliseconds of current second */
    uint16_t ms;
    /** Name of current month */
    const char* month_name;
    /** Name of the current day of the week */
    const char* day_name;
} human_time_t;

/**
 * Initializes the RTC. 
 * @param unix_sec Seconds since unix epoch.
 * @param u_sec Microseconds of current second(<1000000).
 */
void timer_init(uint32_t unix_sec, int32_t u_sec);

/**
 * @returns the current value of the tick counter.
 */
uint32_t timer_get_ticks();

/**
 * @returns Current value of systick counter, which counts cpu cycles.
 */
uint32_t timer_get_subticks();

/**
 * @returns elapsed milliseconds since unix epoch.
 */
int64_t timer_get_ms();

/**
 * @returns if you slept now this amount of ticks, the delay would be \p usec.
 */
uint32_t timer_calc_sleep_until_ticks(uint32_t usec);

/**
 * Sleeps the specified number of ticks.
 */
void timer_sleep_until_ticks(uint32_t ticks);

/**
 * Sleep the specified number of microseconds.
 * Clock resolution is of about 3.33 ms.
 * timer_sleep* functions shouldn't be used for waiting more than an hour.
 */
void timer_sleep_usec(uint32_t usec);

/**
 * Sleep the specified number of milliseconds.
 */
void timer_sleep_msec(uint32_t msec);

/**
 * Sleep the specified number of seconds.
 */
void timer_sleep_sec(uint32_t sec);

/**
 * Gets you the current time (from the RTC) in a human readable format.
 */
human_time_t timer_get_human_time();

#endif /*BLFL_TIMERS*/