#include "control.h"

#define MAIN_SERIAL_NUMBER_ID __COUNTER__
#define MAIN_CLOCK_ID __COUNTER__
#define MAIN_CORE_TEMP_ID __COUNTER__

display_string_t main_serial_number = (display_string_t){"UNINITIALIZED"};
display_time_t main_clock = (display_time_t){0};
display_temp_t main_core_temp = (display_temp_t){0, 0, 0, 0, 0, 1, 1, 0};

static const control_registry_t CONTROL_REGISTRY[] = {
  {CONTROL_DISPLAY_STRING, MAIN_SERIAL_NUMBER_ID, (control_state_t*)&main_serial_number, NULL, NULL, NULL, NULL},
  {CONTROL_DISPLAY_TIME, MAIN_CLOCK_ID, (control_state_t*)&main_clock, NULL, NULL, NULL, NULL},
  {CONTROL_DISPLAY_TEMP, MAIN_CORE_TEMP_ID, (control_state_t*)&main_core_temp, NULL, NULL, NULL, NULL}
};

const control_registry_t* get_control_registry() {
  return CONTROL_REGISTRY;
}

size_t get_control_registry_count() {
  return sizeof(CONTROL_REGISTRY) / sizeof(control_registry_t);
}

size_t get_state_size_from_type(ControlType type) {
  switch(type) {
    case CONTROL_INVALID:
      return 0;
    case CONTROL_DISPLAY_STRING:
      return sizeof(display_string_t);
    case CONTROL_DISPLAY_INTEGER:
      return sizeof(display_integer_t);
    case CONTROL_DISPLAY_DECIMAL:
      return sizeof(display_decimal_t);
    case CONTROL_DISPLAY_TIME:
      return sizeof(display_time_t);
    case CONTROL_DISPLAY_TEMP:
      return sizeof(display_temp_t);
    case CONTROL_EDIT_BUTTON:
      return sizeof(edit_button_t);
    case CONTROL_EDIT_SLIDER:
      return sizeof(edit_slider_t);
    case CONTROL_EDIT_COLOR:
      return sizeof(edit_color_t);
    default:
      return 0;
  }
}