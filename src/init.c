/**
 * This file is responsible for configuring the core.
 */

#include <cm3ex.h>
#include <com.h>
#include <control.h>
#include <loop.h>
#include <power.h>
#include <timers.h>

#include <cm3/cortex.h>
#include <cm3/vector.h>

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <control_resources.h>

blfl_stats_t stats;

/**
 * Entry point.
 * Gets called from reset handler.
 */
int main() {
  // Move vector table to ram.
  static vector_table_t ram_table __attribute__((aligned(512)));
  ram_table = **vector_table_offset;
  *vector_table_offset = &ram_table;
  // Core bootup.
  scb_set_priority_grouping(SCB_AIRCR_PRIGROUP_GROUP4_SUB4);
  cm_enable_faults();
  cm_enable_interrupts();
  // Peripheral bootup.
  power_clockup();
  status_led_init();
  com_init();
  // Sync with master.
  while(1) {
    sync_t sync_data = com_wait_sync();
    if((sync_data.timestamp_seconds ^ sync_data.timestamp_microsec) == sync_data.timestamp_xor) {
      com_ack(COM_FLAGS_DEFAULT, 1);
      com_flush(COM_FLAGS_DEFAULT);
      timer_init(sync_data.timestamp_seconds, sync_data.timestamp_microsec);
      break;
    } else {
      com_ack(COM_FLAGS_DEFAULT, 0);
      com_flush(COM_FLAGS_DEFAULT);
    }
  }
  // Send capabilities to master.
  const control_registry_t* const registry = get_control_registry();
  const size_t registry_len = get_control_registry_count();
  for(int i = registry_len - 1; i >= 0; i--) {
    const control_registry_t* cur_reg = registry + i;
    control_packet_t init_packet = CONTROL_PACKET_CONSTRUCT(cur_reg->type, cur_reg->uid, cur_reg->state);
    com_update_bulk(&init_packet);
  }
  com_update_bulk(NULL);
  // Send display information to master.
  com_bulk(COM_FLAGS_DEFAULT, 1, 0, "control:display", CONTROL_EX_DATA, sizeof(CONTROL_EX_DATA));
  // Run initialization routines.
  for(size_t i = 0; i < registry_len; i++) {
    const control_registry_t* cur_reg = registry + i;
    if(cur_reg->init_callback != NULL) {
      cur_reg->init_callback(cur_reg);
    }
  }
  // Enter scheduler/ticker/main loop.
  main_loop();
  // Run de-initialization routines.
  for(size_t i = 0; i < registry_len; i++) {
    const control_registry_t* cur_reg = registry + i;
    if(cur_reg->deinit_callback != NULL) {
      cur_reg->deinit_callback(cur_reg);
    }
  }
  // If we reach this point, user intervention is required.
  while(1) {
    status_led_blink(1, 5);
  }
  com_deinit();
  return EXIT_SUCCESS;
}