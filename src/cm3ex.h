#ifndef BLFL_CM3EX
#define BLFL_CM3EX
/**
 * Extra features on top of libopencm3.
 */

#include <cm3/nvic.h>
#include <cm3/scb.h>
#include <cm3/vector.h>

static vector_table_t** const vector_table_offset = (vector_table_t** const)&SCB_VTOR;
static inline void nvic_set_correct_priority(uint8_t irqn, uint8_t priority) {
  nvic_set_priority(irqn, priority * 16);
}

#endif /*BLFL_CM3EX*/