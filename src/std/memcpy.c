#include <stdint.h>
#include <string.h>

void* memcpy(void* restrict dst, const void* restrict src, size_t n) {
  uint32_t* plDst = (uint32_t*)dst;
  uint32_t const* plSrc = (uint32_t const*)src;

  if(!(((uintptr_t)src) & 0xFFFFFFFC) && !(((uintptr_t)dst) & 0xFFFFFFFC)) {
    while(n >= 4) {
      *plDst++ = *plSrc++;
      n -= 4;
    }
  }

  uint8_t* pcDst = (uint8_t*)plDst;
  uint8_t const* pcSrc = (uint8_t const*)plSrc;

  while(n--) {
    *pcDst++ = *pcSrc++;
  }

  return dst;
}