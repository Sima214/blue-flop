#include <limits.h>
#include <string.h>
/**
 * Taken from newlib.
 * TODO: license stuff
 */

#define unaligned(a) ((long)a & (sizeof(long) - 1))

#if LONG_MAX == 2147483647L
  #define detectnull(a) (((a)-0x01010101) & ~(a)&0x80808080)
#elif LONG_MAX == 9223372036854775807L
  /* Nonzero if X (a long int) contains a NULL byte. */
  #define detectnull(a) (((a)-0x0101010101010101) & ~(a)&0x8080808080808080)
#else
  #error long int is not a 32bit or 64bit type.
#endif

size_t strlen(const char* str) {
  const char* start = str;
  unsigned long* aligned_addr;

  /* Align the pointer, so we can search a word at a time.  */
  while(unaligned(str)) {
    if(!*str)
      return str - start;
    str++;
  }

  /*
   * If the string is word-aligned, we can check for the presence of
   * a null in each word-sized block.
   */
  aligned_addr = (unsigned long*)str;
  while(!detectnull(*aligned_addr)) {
    aligned_addr++;
  }

  /*
   * Once a null is detected, we check each byte in that block for a
   * precise position of the null.
   */
  str = (char*)aligned_addr;
  while(*str) {
    str++;
  }
  return str - start;
}