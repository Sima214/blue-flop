#include <stddef.h>
#include <stdint.h>
#include <string.h>

void* memset(void* p, int v, size_t n) {
  uint8_t v8 = v;
  uint32_t v32 = v8 | v8 << 8 | v8 << 16 | v8 << 24;
  // Head set.
  uint8_t* p8 = p;
  while(n > 0 && (((uintptr_t)p8) % sizeof(void*)) != 0) {
    *p8 = v8;
    p8++;
    n--;
  }
  // Word set.
  uint32_t* p32 = (uint32_t*)p8;
  while(n >= sizeof(void*)) {
    *p32 = v32;
    p32++;
    n -= sizeof(void*);
  }
  // Tail set.
  p8 = (uint8_t*)p32;
  while(n != 0) {
    *p8 = v8;
    p8++;
    n--;
  }
  return p;
}
