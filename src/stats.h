#ifndef BLFL_STATS
#define BLFL_STATS

#include <stddef.h>
#include <stdint.h>

/**
 * Core statistics.
 */
typedef struct {
  size_t ram_usage;
} mem_stats_t;

/**
 * Status tracking for dma/uart.
 */
typedef struct {
  size_t sent_bytes;
  size_t received_bytes;
  uint32_t sent_packets;
  uint32_t received_packets;
  uint16_t error_received_fatal;
  uint16_t error_parity;
  uint16_t error_overrun;
  uint16_t error_dma;
} com_stats_t;

typedef struct {
  mem_stats_t mem;
  com_stats_t com;
} blfl_stats_t;

extern blfl_stats_t stats;

/**
 * Initial packet received from master.
 */
typedef struct {
  uint32_t timestamp_seconds;
  int32_t timestamp_microsec;
  uint32_t timestamp_xor;
} sync_t;

#endif /*BLFL_STATS*/