#ifndef BLFL_MOTOR
#define BLFL_MOTOR

#include <state.h>

#define MOTOR_DEFAULT_FREQ 224

#define STALL_DUTY_DEFAULT 70
#define START_DUTY_DEFAULT 180
#define RESON_DUTY_DEFAULT 770

int motor_init(const options_t* self);

int motor_deinit(const options_t* self);

int motor_set_duty(const options_t* self, const update_t* update);

int motor_set_freq(const options_t* self, const update_t* update);

int motor_set_intensity(const options_t* self, const update_t* update);

int motor_set_impulse(const options_t* self, const update_t* update);

int motor_set_stall(const options_t* self, const update_t* update);
int motor_set_start(const options_t* self, const update_t* update);
int motor_set_reson(const options_t* self, const update_t* update);
int motor_set_intensity_mod(const options_t* self, const update_t* update);

#endif /*BLFL_MOTOR*/