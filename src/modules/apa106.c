#include "apa106.h"

#include <com.h>
#include <macros.h>

#include <cm3/nvic.h>
#include <stm32/dma.h>
#include <stm32/gpio.h>
#include <stm32/timer.h>

#include <stdint.h>

#define PWM_PERIOD 62
#define FAST_TIME 13
#define SLOW_TIME 49
#define APA_106_ZERO FAST_TIME
#define APA_106_ONE SLOW_TIME
#define LED_COUNT 2

typedef struct {
  int l1_red : 8;
  int l1_green : 8;
  int l1_blue : 8;
  int l2_red : 8;
  int l2_green : 8;
  int l2_blue : 8;
} apa106_state_t;

// Each led requires 24 bits, and 32 for 50μs reset signal.
static uint8_t dma_led_buffer[LED_COUNT * 24 + 32];
static apa106_state_t apa106_state = {0, 0, 255, 0, 0, 255};
static int volatile transferring = 0;

void dma1_channel7_isr() {
  if(hot_branch((DMA1_ISR & DMA_ISR_TCIF7) != 0)) {
    DMA1_IFCR |= DMA_IFCR_CTCIF7;
    transferring = 0;
    // Partial reset.
    timer_disable_counter(TIM4);
    dma_disable_channel(DMA1, DMA_CHANNEL7);
    timer_set_prescaler(TIM4, 0);
    timer_set_repetition_counter(TIM4, 0);
    timer_set_oc_value(TIM4, TIM_OC4, 0);
  }
}

int apa106_init(unused const options_t* self) {
  // Setup pwm.
  gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM4_CH4);
  timer_set_mode(TIM4, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  // Reset counters.
  timer_set_prescaler(TIM4, 0);
  timer_set_repetition_counter(TIM4, 0);
  timer_set_oc_value(TIM4, TIM_OC4, 0);
  // Set frequency.
  timer_set_period(TIM4, PWM_PERIOD);
  timer_enable_update_event(TIM4);
  timer_enable_preload(TIM4);
  timer_continuous_mode(TIM4);
  // Enable output.
  timer_set_oc_mode(TIM4, TIM_OC4, TIM_OCM_PWM1);
  timer_disable_oc_clear(TIM4, TIM_OC4);
  timer_enable_oc_preload(TIM4, TIM_OC4);
  timer_set_oc_fast_mode(TIM4, TIM_OC4);
  timer_enable_oc_output(TIM4, TIM_OC4);
  // DMA preparations.
  timer_enable_irq(TIM4, TIM_DIER_UDE);
  timer_set_dma_on_update_event(TIM4);
  dma_channel_reset(DMA1, DMA_CHANNEL7);
  // TIM4_UP on DMA channel 7.
  nvic_set_priority(NVIC_DMA1_CHANNEL7_IRQ, 1);
  nvic_enable_irq(NVIC_DMA1_CHANNEL7_IRQ);
  dma_set_peripheral_address(DMA1, DMA_CHANNEL7, (uintptr_t)&TIM4_CCR4);
  dma_set_read_from_memory(DMA1, DMA_CHANNEL7);
  dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL7);
  dma_enable_circular_mode(DMA1, DMA_CHANNEL7);
  dma_set_peripheral_size(DMA1, DMA_CHANNEL7, DMA_CCR_PSIZE_16BIT);
  dma_set_memory_size(DMA1, DMA_CHANNEL7, DMA_CCR_MSIZE_8BIT);
  dma_set_priority(DMA1, DMA_CHANNEL7, DMA_CCR_PL_HIGH);
  dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL7);
  // Finish up.
  return 0;
}

int apa106_update(const options_t* self, const update_t* update) {
  if(self->uid == 3) {
    apa106_state.l1_red = (update->color.red * update->color.intensity) / 255;
    apa106_state.l1_green = (update->color.green * update->color.intensity) / 255;
    apa106_state.l1_blue = (update->color.blue * update->color.intensity) / 255;
  } else if(self->uid == 4) {
    apa106_state.l2_red = (update->color.red * update->color.intensity) / 255;
    apa106_state.l2_green = (update->color.green * update->color.intensity) / 255;
    apa106_state.l2_blue = (update->color.blue * update->color.intensity) / 255;
  }
  // Nop if currently transferring.
  if(cold_branch(transferring)) {
    com_updt_flush();
    com_msg_start();
    com_msg_string("Race condition: already setting colors!");
    com_msg_end();
    return 1;
  }
  // Generate waveform.
  uint8_t* rgb_data = (uint8_t*)&apa106_state;
  for(int32_t i = LED_COUNT * 24; i >= 1; i--) {
    size_t word = i / 8;
    unsigned int bit = 7 - (i % 8);
    size_t mask = ((size_t)0x1) << bit;
    int value = rgb_data[word] & mask;
    value = !!value;
    dma_led_buffer[i] = value ? APA_106_ONE : APA_106_ZERO;
  }
  // Setup dma.
  dma_set_memory_address(DMA1, DMA_CHANNEL7, (uintptr_t)dma_led_buffer);
  dma_set_number_of_data(DMA1, DMA_CHANNEL7, sizeof(dma_led_buffer));
  // Start dma and timer.
  transferring = 1;
  dma_enable_channel(DMA1, DMA_CHANNEL7);
  timer_enable_counter(TIM4);
  return 0;
}