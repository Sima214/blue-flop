#include "motor.h"

#include <com.h>
#include <macros.h>
#include <state.h>

#include <stm32/gpio.h>
#include <stm32/timer.h>

#include <stdint.h>

static int duty = 0;
static int intensity = 0;
static int intensity_mod = 1000;
static int STALL_DUTY = STALL_DUTY_DEFAULT;
static int START_DUTY = START_DUTY_DEFAULT;
static int RESON_DUTY = RESON_DUTY_DEFAULT;

int motor_init(unused const options_t* self) {
  gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM4_CH4);
  timer_set_mode(TIM4, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  // Reset counters.
  timer_set_oc_value(TIM4, TIM_OC4, 0);
  // Set frequency.
  timer_set_prescaler(TIM4, MOTOR_DEFAULT_FREQ);
  timer_set_period(TIM4, 1023);
  timer_enable_preload(TIM4);
  timer_continuous_mode(TIM4);
  // Enable outputs.
  timer_set_oc_mode(TIM4, TIM_OC4, TIM_OCM_PWM2);
  timer_disable_oc_clear(TIM4, TIM_OC4);
  timer_enable_oc_preload(TIM4, TIM_OC4);
  timer_set_oc_fast_mode(TIM4, TIM_OC4);
  timer_enable_oc_output(TIM4, TIM_OC4);
  // Start motor pwm.
  timer_enable_counter(TIM4);
  return 0;
}

int motor_deinit(unused const options_t* self) {
  timer_set_oc_value(TIM4, TIM_OC4, 0);
  timer_disable_counter(TIM4);
  duty = 0;
  return 0;
}

static void update_duty() {
  int actual_duty = (intensity_mod * duty) / 1000;
  timer_set_oc_value(TIM4, TIM_OC4, actual_duty);
  update_t packet = {LENGTH_UPDATE_SLIDER_VALUE_ONLY,
                     CONTROL_EDIT_SLIDER,
                     3,
                     .slider = {actual_duty}};
  com_updt_append(&packet);
}

int motor_set_duty(unused const options_t* self, const update_t* update) {
  duty = update->slider.value;
  timer_set_oc_value(TIM4, TIM_OC4, duty);
  return 0;
}

int motor_set_freq(unused const options_t* self, const update_t* update) {
  timer_disable_counter(TIM4);
  timer_set_prescaler(TIM4, update->slider.value);
  update_duty();
  timer_enable_counter(TIM4);
  return 0;
}

static int motor_kickstart(unused void* data) {
  if(duty == START_DUTY) {
    duty = STALL_DUTY + ((START_DUTY - STALL_DUTY) * intensity) / 300;
  }
  update_duty();
  return 0;
}

int motor_set_intensity(unused const options_t* self, const update_t* update) {
  intensity = update->slider.value;
  if(intensity == 0) {
    duty = 0;
  } else if(intensity <= 300) {
    // Stall to start speed.
    if(duty == 0) {
      // Kickstart.
      duty = START_DUTY;
      register_tick_callback(100, motor_kickstart, NULL);
    } else {
      duty = STALL_DUTY + ((START_DUTY - STALL_DUTY) * intensity) / 300;
    }
  } else if(intensity <= 600) {
    // Start to reson speed.
    duty = START_DUTY + ((RESON_DUTY - START_DUTY) * (intensity - 300)) / 300;
  } else if(intensity <= 1000) {
    // Reson to max speed.
    duty = RESON_DUTY + ((1023 - RESON_DUTY) * (intensity - 600)) / 400;
  } else {
    return 1;
  }
  update_duty();
  return 0;
}

static int impulse_old_duty = 0;

static int motor_impulse(unused void* data) {
  if(intensity == 0) {
    return 0;
  } else if(duty == 0 && impulse_old_duty < START_DUTY) {
    duty = START_DUTY;
    update_duty();
    return 1;
  } else {
    duty = impulse_old_duty;
    update_duty();
    return 0;
  }
}

int motor_set_impulse(unused const options_t* self, const update_t* update) {
  static int impulse_old_state = 0;
  int impulse_new_state = update->slider.value;
  if(impulse_new_state != impulse_old_state) {
    impulse_old_state = impulse_new_state;
    impulse_old_duty = duty;
    duty = 0;
    update_duty();
    register_tick_callback(100, motor_impulse, NULL);
  }
  return 0;
}

int motor_set_stall(unused const options_t* self, const update_t* update) {
  STALL_DUTY = update->slider.value;
  update_duty();
  return 0;
}

int motor_set_start(unused const options_t* self, const update_t* update) {
  START_DUTY = update->slider.value;
  update_duty();
  return 0;
}

int motor_set_reson(unused const options_t* self, const update_t* update) {
  RESON_DUTY = update->slider.value;
  update_duty();
  return 0;
}

int motor_set_intensity_mod(unused const options_t* self, const update_t* update) {
  intensity_mod = update->slider.value;
  update_duty();
  return 0;
}