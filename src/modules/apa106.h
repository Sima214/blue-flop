#ifndef BLFL_APA106
#define BLFL_APA106

#include <state.h>

int apa106_init(const options_t* self);

int apa106_update(const options_t* self, const update_t* update);

#endif /*BLFL_APA106*/