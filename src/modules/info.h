#ifndef BLFL_INFO
#define BLFL_INFO

#include <state.h>

int board_info_init(const options_t* self);

int board_timer_init(const options_t* self);

int test_button1(const options_t* self, const update_t* update);

int test_button2(const options_t* self, const update_t* update);

int test_slider(const options_t* self, const update_t* update);

#endif /*BLFL_INFO*/