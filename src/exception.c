#include <macros.h>
#include <power.h>

#include <stm32/gpio.h>
#include <stm32/rcc.h>
#include <stm32/usart.h>
#include <cm3/scb.h>

#include <stddef.h>
#include <stdint.h>

typedef struct {
  uint32_t header;
  uint32_t r0;
  uint32_t r1;
  uint32_t r2;
  uint32_t r3;
  uint32_t r12;
  uint32_t lr;
  uint32_t pc;
  uint32_t psr;
  uint32_t bfar;
  uint32_t cfsr;
  uint32_t hfsr;
  uint32_t dfsr;
  uint32_t afsr;
  uint32_t scb_shcsr;
  uint32_t end;
} fault_t;

/**
 * Hard fault handlers.
 * 
 * Now with minimal reporting.
 */
naked_interrupt void hard_fault_handler() {
  uint32_t* hardfault_args;
  __asm volatile(
    "tst lr, #4                        \n"
    "ite eq                            \n"
    "mrseq %0, msp                     \n"
    "mrsne %0, psp                     \n"
    : "=r"(hardfault_args));
  fault_t fault;
  fault.header = sizeof(fault);
  fault.end = 0x0d00000a;
  fault.r0 = hardfault_args[0];
  fault.r1 = hardfault_args[1];
  fault.r2 = hardfault_args[2];
  fault.r3 = hardfault_args[3];
  fault.r12 = hardfault_args[4];
  fault.lr = hardfault_args[5];
  fault.pc = hardfault_args[6];
  fault.psr = hardfault_args[7];
  fault.bfar = SCB_BFAR;
  fault.cfsr = SCB_CFSR;
  fault.hfsr = SCB_HFSR;
  fault.dfsr = SCB_DFSR;
  fault.afsr = SCB_AFSR;
  fault.scb_shcsr = SCB_SHCSR;
  power_panic_reset();
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_USART1);
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);
  usart_set_baudrate(USART1, 72000000, 9600);
  usart_set_mode(USART1, USART_MODE_TX);
  usart_set_databits(USART1, 8);
  usart_set_stopbits(USART1, USART_STOPBITS_1);
  usart_set_parity(USART1, USART_PARITY_NONE);
  usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
  usart_enable(USART1);
  while(1) {
    status_led_blink(1, 4);
    uint8_t* packet = (uint8_t*)(&fault);
    for(size_t i = 0; i < sizeof(fault); i++) {
      usart_send_blocking(USART1, packet[i]);
    }
    usart_wait_send_ready(USART1);
  }
  unreachable();
}

interrupt void mem_manage_handler() {
  power_panic_reset();
  while(1) {
    status_led_blink(1, 3);
  }
  unreachable();
}

interrupt void bus_fault_handler() {
  power_panic_reset();
  while(1) {
    status_led_blink(1, 2);
  }
  unreachable();
}

interrupt void usage_fault_handler() {
  power_panic_reset();
  while(1) {
    status_led_blink(1, 1);
  }
  unreachable();
}