#include "codecs.h"

#include <macros.h>

#include <stddef.h>
#include <stdint.h>

#include "generated/byte2hex.h"

void hex_encode(char* dest, const void* data, size_t n) {
  const uint8_t* data8 = data;
  for(size_t i = 0; i < n; i++) {
    uint8_t v = data8[i];
    dest[i * 2] = BYTE2HEX[v][0];
    dest[i * 2 + 1] = BYTE2HEX[v][1];
  }
}

int hex_decode(void* data, const char* src, size_t n);

#include "generated/crc.h"

uint32_t calc_crc(const void* data_in, size_t n) {
  const uint8_t* data = data_in;
  uint32_t crc = 0xffffffff;
  // Automatically generated CRC function
  // polynomial: 0x104C11DB7, bit reverse algorithm
  while(n > 0) {
    crc = CRC_TABLE[*data ^ (uint8_t)crc] ^ (crc >> 8);
    data++;
    n--;
  }
  return crc;
}
