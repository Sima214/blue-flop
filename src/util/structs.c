#include "structs.h"

#include <stddef.h>
#include <string.h>

#define heap_parent(i) ((i - 1) / 2)
#define heap_left(i) (2 * i + 1)
#define heap_right(i) (2 * i + 2)

void priority_queue_create(priority_queue_t* dst, PRIORITY_QUEUE_TYPE* data, size_t n) {
  memset(data, 0, n * sizeof(PRIORITY_QUEUE_TYPE));
  dst->array = data;
  dst->length = 0;
  dst->size = n;
}

int priority_queue_insert(priority_queue_t* dst, PRIORITY_QUEUE_TYPE e) {
  if(dst->length == dst->size) {
    return 1;
  }
  dst->array[dst->length] = e;
  // Sift-up:
  size_t i = dst->length;
  size_t p = heap_parent(i);
  while(i != 0 && dst->array[i] < dst->array[p]) {
    PRIORITY_QUEUE_TYPE tmp = dst->array[i];
    dst->array[i] = dst->array[p];
    dst->array[p] = tmp;
    i = p;
    p = heap_parent(i);
  }
  dst->length++;
  return 0;
}

int priority_queue_peek(priority_queue_t* dst, PRIORITY_QUEUE_TYPE* e) {
  if(dst->length > 0) {
    *e = dst->array[0];
    return 0;
  } else {
    return 1;
  }
}

int priority_queue_delete(priority_queue_t* dst) {
  if(dst->length == 0) {
    return 1;
  }
  dst->length--;
  if(dst->length == 0) {
    return 0;
  } else {
    dst->array[0] = dst->array[dst->length];
    // Sift-down:
    size_t root = 0;
    size_t swap = root;
    size_t left = heap_left(root);
    size_t right = heap_right(root);
    while(left < dst->length) {
      if(dst->array[left] < dst->array[swap]) {
        swap = left;
      }
      if(right < dst->length && dst->array[right] < dst->array[swap]) {
        swap = right;
      }
      if(root == swap) {
        return 0;
      } else {
        PRIORITY_QUEUE_TYPE old_root = dst->array[root];
        dst->array[root] = dst->array[swap];
        dst->array[swap] = old_root;
        root = swap;
        left = heap_left(root);
        right = heap_right(root);
      }
    }
  }
  return 0;
}