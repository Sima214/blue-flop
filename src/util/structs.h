#ifndef BLFL_STRUCTS
#define BLFL_STRUCTS

#include <stddef.h>
#include <stdint.h>

#define PRIORITY_QUEUE_TYPE uint32_t

typedef struct {
  PRIORITY_QUEUE_TYPE* array;
  size_t length;
  size_t size;
} priority_queue_t;

/**
 * Creates an empty priority queue and stores the object in \p dst.
 */
void priority_queue_create(priority_queue_t* dst, PRIORITY_QUEUE_TYPE* data, size_t n);

/**
 * Returns non-zero when an error occurs, like the queue beeing full.
 */
int priority_queue_insert(priority_queue_t* dst, PRIORITY_QUEUE_TYPE e);

/**
 * Returns non-zero when an error occurs, like the queue beeing empty.
 */
int priority_queue_peek(priority_queue_t* dst, PRIORITY_QUEUE_TYPE* e);

/**
 * Deletes the root.
 * 
 * Returns non-zero when an error occurs, like the queue beeing empty.
 */
int priority_queue_delete(priority_queue_t* dst);

#endif /*BLFL_STRUCTS*/