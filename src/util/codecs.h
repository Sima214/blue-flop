#ifndef BLFL_CODECS
#define BLFL_CODECS
/**
 * Encoding/Decoding/Compression
 */

#include <stddef.h>
#include <stdint.h>

/**
 * @param n length of \p data in bytes.
 */
void hex_encode(char* dest, const void* data, size_t n);

/**
 * @param n length of \p src in bytes.
 */
int hex_decode(void* data, const char* src, size_t n);

uint32_t calc_crc(const void* data, size_t n);

#endif /*BLFL_CODECS*/