#include "power.h"

#include <macros.h>

#include <cm3/scb.h>
#include <stm32/adc.h>
#include <stm32/flash.h>
#include <stm32/gpio.h>
#include <stm32/rcc.h>
#include <stm32/usart.h>

#include <stdint.h>

#define SCR ((uint32_t*)0xE000ED10)

int volatile power_sleep_reason = -1;

void power_clockup() {
  // Enable external oscillator.
  rcc_osc_on(RCC_HSE);

  /*
   * Set prescalers for:
   *  - AHB  : 72MHz(72MHz max)
   *  - ABP1 : 36MHz(36MHz max)
   *  - ABP2 : 72MHz(72MHz max)
   *  - ADC  : 12MHz(14MHz max)
   *  - USB  : 48MHz
   * 
   * We need to do this now, before we've switched to the PLL.
	 */
  rcc_set_hpre(RCC_CFGR_HPRE_SYSCLK_NODIV);
  rcc_set_ppre1(RCC_CFGR_PPRE1_HCLK_DIV2);
  rcc_set_ppre2(RCC_CFGR_PPRE2_HCLK_NODIV);
  rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV6);
  rcc_set_usbpre(RCC_CFGR_USBPRE_PLL_CLK_DIV1_5);
  // Sysclk will run with 72MHz -> 2 waitstates.
  flash_set_ws(FLASH_ACR_LATENCY_2WS);

  // Wait until external oscillator is ready before we start messing with the PLL.
  rcc_wait_for_osc_ready(RCC_HSE);
  // Set the PLL multiplication factor to 9.
  rcc_set_pll_multiplication_factor(RCC_CFGR_PLLMUL_PLL_CLK_MUL9);
  // Select HSE as PLL source.
  rcc_set_pll_source(RCC_CFGR_PLLSRC_HSE_CLK);
  // External frequency undivided before entering PLL.
  rcc_set_pllxtpre(RCC_CFGR_PLLXTPRE_HSE_CLK);
  // Enable PLL oscillator and wait for it to stabilize.
  rcc_osc_on(RCC_PLL);
  rcc_wait_for_osc_ready(RCC_PLL);

  // Select PLL as SYSCLK source.
  rcc_set_sysclk_source(RCC_CFGR_SW_SYSCLKSEL_PLLCLK);

  // Enable all peripherals that might be used.
  rcc_periph_clock_enable(RCC_DMA1);
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_GPIOC);
  rcc_periph_clock_enable(RCC_AFIO);
  rcc_periph_clock_enable(RCC_USART1);
  rcc_periph_clock_enable(RCC_TIM1);
  rcc_periph_clock_enable(RCC_TIM2);
  rcc_periph_clock_enable(RCC_TIM3);
  rcc_periph_clock_enable(RCC_TIM4);
  rcc_periph_clock_enable(RCC_ADC1);
  rcc_periph_clock_enable(RCC_ADC2);
}

void power_panic_reset() {
  // Hold.
  rcc_periph_reset_hold(RCC_DMA1);
  rcc_periph_reset_hold(RCC_GPIOA);
  rcc_periph_reset_hold(RCC_GPIOB);
  rcc_periph_reset_hold(RCC_GPIOC);
  rcc_periph_reset_hold(RCC_AFIO);
  rcc_periph_reset_hold(RCC_USART1);
  rcc_periph_reset_hold(RCC_TIM1);
  rcc_periph_reset_hold(RCC_TIM2);
  rcc_periph_reset_hold(RCC_TIM3);
  rcc_periph_reset_hold(RCC_TIM4);
  rcc_periph_reset_hold(RCC_ADC1);
  rcc_periph_reset_hold(RCC_ADC2);
  // Release.
  rcc_periph_reset_release(RCC_DMA1);
  rcc_periph_reset_release(RCC_GPIOA);
  rcc_periph_reset_release(RCC_GPIOB);
  rcc_periph_reset_release(RCC_GPIOC);
  rcc_periph_reset_release(RCC_AFIO);
  rcc_periph_reset_release(RCC_USART1);
  rcc_periph_reset_release(RCC_TIM1);
  rcc_periph_reset_release(RCC_TIM2);
  rcc_periph_reset_release(RCC_TIM3);
  rcc_periph_reset_release(RCC_TIM4);
  rcc_periph_reset_release(RCC_ADC1);
  rcc_periph_reset_release(RCC_ADC2);
  // Re-enable status led.
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
}

void status_led_init() {
  // Connected to a green led in the blue pill board.
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
  status_led_on();
}

void status_led_on() {
  gpio_clear(GPIOC, GPIO13);
}

void status_led_off() {
  gpio_set(GPIOC, GPIO13);
}

void status_led_blink(int slow, uint32_t count) {
  status_led_off();
  for(int i = 0; i < 1200000; i++) {
    nop;
  }
  for(/* NOP */; count != 0; count--) {
    status_led_on();
    for(int i = 0; i < (slow ? 2000000 : 1400000); i++) {
      nop;
    }
    status_led_off();
    for(int i = 0; i < (slow ? 1600000 : 800000); i++) {
      nop;
    }
  }
  for(int i = 0; i < 2400000; i++) {
    nop;
  }
  status_led_on();
}

int power_sleep() {
  status_led_off();
  *SCR |= SCB_SCR_SLEEPONEXIT;
  __asm__("wfi");
  status_led_on();
  return power_sleep_reason;
}

void power_wakeup(int reason) {
  // Clear SLEEPONEXIT bit.
  *SCR &= ~SCB_SCR_SLEEPONEXIT;
  power_sleep_reason = reason;
}