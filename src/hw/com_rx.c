#include "com.h"

#include <macros.h>
#include <power.h>
#include <stats.h>

#include <stm32/usart.h>

#include <stdint.h>
#include <string.h>

typedef struct {
  int ack_flag : 1;
  int ack_wait : 1;
  int sync_flag : 1;
  int sync_wait : 1;
  int exit_requested : 1;
  int8_t ack_status_byte;
  sync_t sync_data;
} com_rx_state_t;

static uint8_t receive_buffer[256];
static size_t receive_buffer_pos = 0;
static volatile com_rx_state_t com_rx_state;

void usart_mute(uint32_t usart) {
  uint32_t reg32 = USART_CR1(usart);
  reg32 |= USART_CR1_RWU;
  USART_CR1(usart) = reg32;
}

/**
 * Resets buffer position and increments stats.
 */
static void com_packet_received() {
  receive_buffer_pos = 0;
  stats.com.received_packets++;
}

static void com_fatal_received() {
  usart_mute(USART1);
  receive_buffer_pos = 0;
  stats.com.error_received_fatal++;
}

static void com_case_ack() {
  if(receive_buffer_pos == 2) {
    com_rx_state.ack_flag = 1;
    com_rx_state.ack_status_byte = receive_buffer[1];
    if(com_rx_state.ack_wait) {
      power_wakeup(2);
    }
    com_packet_received();
  }
}

static void com_case_sync() {
  if(receive_buffer_pos == sizeof(char) + sizeof(sync_t)) {
    sync_t* sync = (sync_t*)&receive_buffer[1];
    com_rx_state.sync_data = *sync;
    com_rx_state.sync_flag = 1;
    if(com_rx_state.sync_wait) {
      power_wakeup(2);
    }
    com_packet_received();
  }
}

static void com_case_exit() {
  com_rx_state.exit_requested = 1;
  com_packet_received();
}

interrupt void usart1_isr() {
  if(usart_get_flag(USART1, USART_SR_RXNE)) {
    // Check for overflow.
    if(receive_buffer_pos == sizeof(receive_buffer)) {
      com_fatal_received();
      return;
    }
    // Collect error statistics.
    int error_parity = usart_get_flag(USART1, USART_SR_PE);
    int error_overrun = usart_get_flag(USART1, USART_SR_ORE);
    stats.com.error_parity += error_parity;
    stats.com.error_overrun += error_overrun;
    // Append to buffer.
    receive_buffer[receive_buffer_pos] = (uint8_t)usart_recv(USART1);
    receive_buffer_pos++;
    stats.com.received_bytes++;
    // Consume input.
    if(receive_buffer[0] == COM_ID_ACK) {
      com_case_ack();
    } else if(receive_buffer[0] == COM_ID_STA) {
      com_case_sync();
    } else if(receive_buffer[0] == COM_ID_STA) {
      com_case_exit();
    } else {
      com_fatal_received();
    }
  }
}

int8_t com_wait_ack() {
  com_rx_state.ack_wait = 1;
  while(com_rx_state.ack_flag == 0) {
    power_sleep();
  }
  com_rx_state.ack_flag = 0;
  com_rx_state.ack_wait = 0;
  return com_rx_state.ack_status_byte;
}

sync_t com_wait_sync() {
  com_rx_state.sync_wait = 1;
  while(com_rx_state.sync_flag == 0) {
    power_sleep();
  }
  com_rx_state.sync_wait = 0;
  com_rx_state.sync_flag = 0;
  return com_rx_state.sync_data;
}

int com_check_exit() {
  if(com_rx_state.exit_requested) {
    com_rx_state.exit_requested = 0;
    return 1;
  }
  return 0;
}