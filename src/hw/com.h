#ifndef BLFL_COM
#define BLFL_COM
/**
 * Main communication module.
 * Input is handled with interrupts.
 * Output is double buffered DMA.
 * 
 * Most functions return an error code,
 * which is any of COM_STATUS_* or(ed),
 * or 0 zero if nothing unexpected happened.
 * 
 * Most functions take a flags argument,
 * which is any of COM_FLAG_* or(ed).
 * 
 * YOU MUST NOT CALL THIS FROM INSIDE AN INTERRUPT(data race problems).
 */

#include <stats.h>
#include <control.h>
#include <macros.h>

#include <stddef.h>
#include <stdint.h>

typedef uint32_t com_error_t;
typedef uint32_t com_flags_t;

/**
 * Null. Not part of the protocol.
 * Used only for state tracking.
 */
#define COM_ID_NUL '\0'
/**
 * Acknowledge with extra status byte.
 */
#define COM_ID_ACK 'A'
/**
 * Bulk transfer.
 * 
 * Compound packet.
 */
#define COM_ID_BLK 'B'
/**
 * Echo/Ping.
 * Structure:
 * 1. One byte id character.
 * 2. uint16_t message length.
 * 3. Message to be echoed.
 */
#define COM_ID_ECH 'E'
/**
 * Echo responce.
 * 
 * Same format as echo.
 */
#define COM_ID_ERS 'R'
/**
 * Exit/Reset.
 * 
 * No packet body.
 */
#define COM_ID_EXI 'X'
/**
 * Message/Log.
 * 
 * Null terminated string.
 */
#define COM_ID_MSG 'M'
/**
 * Status packet when slave->master.
 * Sync packet when master->slave.
 */
#define COM_ID_STA 'S'
/**
 * Update a control unit.
 */
#define COM_ID_UPD 'U'

/**
 * No error.
 */
#define COM_STATUS_OK 0
/**
 * The type of what you are currently sending,
 * does not match what is expected and already in the buffer.
 */
#define COM_STATUS_INVALID_STATE mask_create(0)
/**
 * There was not enough space on the front buffer.
 * Buffers were swapped, unless COM_FLAG_NO_SWAP was specified.
 */
#define COM_STATUS_OVF mask_create(1)
/**
 * COM_FLAG_NO_BLOCK was specified and call would have blocked.
 * Data was not transferred.
 */
#define COM_STATUS_WOULD_BLOCK mask_create(2)
/**
 * A compound packet was denied.
 */
#define COM_STATUS_COMPOUND_DENIED mask_create(3)

/**
 * Default flags.
 */
#define COM_FLAGS_DEFAULT 0
/**
 * If there is not enough space in the front buffer,
 * and the back buffer has not been transfered,
 * do not block waiting for the back buffer to become available.
 */
#define COM_FLAG_NO_BLOCK mask_create(0)
/**
 * Does nothing if we are at an invalid state.
 */
#define COM_FLAG_NOP_INVALID mask_create(1)
/**
 * If new content does not fit,
 * then do not start a new transfer.
 * (by swapping the buffers)
 */
#define COM_FLAG_NO_SWAP mask_create(2)

void com_init();

/**
 * Start a new string message.
 */
com_error_t com_msg_start(com_flags_t);

/**
 * Append any non null character to your message.
 */
com_error_t com_msg_string(com_flags_t flags, const char* str);

/**
 * Append signed numbers to your message.
 */
com_error_t com_msg_i32(com_flags_t flags, int32_t v, int padding);

/**
 * Append unsigned numbers to your message.
 */
com_error_t com_msg_u32(com_flags_t flags, uint32_t v, int padding);

/**
 * Marks the end of your message(similar to a newline).
 */
com_error_t com_msg_end(com_flags_t flags);

#define com_msg_puts(s)                 \
  com_msg_start(COM_FLAGS_DEFAULT);     \
  com_msg_string(COM_FLAGS_DEFAULT, s); \
  com_msg_end(COM_FLAGS_DEFAULT);

/**
 * Send an acknowledge.
 */
com_error_t com_ack(com_flags_t, int8_t);

/**
 * Do a bulk transfer.
 * Note that this is a compound packet, it's impossible not to block for large inputs.
 * 
 * @param crc Also calculate and send crc for each packet?
 * @param comp Compress data before sending it?
 * @param id Null-terminated string with the id of data. Used by the master. Max 31 characters.
 * @param data Block of data.
 * @param n Size of block of data in bytes.
 */
com_error_t com_bulk(com_flags_t flags, int crc, int comp, const char* id, const void* data, size_t n);

/**
 * Send an echo.
 * This is similar to ip echo/ping.
 */
com_error_t com_echo(com_flags_t flags, int8_t id, const void* data, uint16_t n);

/**
 * Inform master that we are closing.
 */
com_error_t com_exit(com_flags_t);

/**
 * Update master with status.
 */
com_error_t com_update_status(com_flags_t flags);

/**
 * Appends a new update packet.
 */
com_error_t com_update_append(com_flags_t flags, const control_packet_t* packet);

/**
 * Send update packets with bulk/ack command.
 * Intented only for the initial send.
 * To finish up call this with null.
 */
com_error_t com_update_bulk(const control_packet_t* packet);

/**
 * Flushes/Swaps buffers.
 * 
 * Note: Some flags are ignored,
 * as they make no sense here.
 */
com_error_t com_flush(com_flags_t flags);

/**
 * Waits until an ach packet has been received.
 */
int8_t com_wait_ack();

/**
 * Waits for a sync packet, used to initialize some structures.
 */
sync_t com_wait_sync();

/**
 * Returns True(1) if an exit packet has been received.
 * Clears the internal flag, so all subsequent calls
 * to this function will return False(1),
 * until a new packet is received.
 */
int com_check_exit();

void com_deinit();

#endif /*BLFL_COM*/