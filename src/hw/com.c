#include "com.h"

#include <cm3ex.h>

#include <cm3/nvic.h>
#include <stm32/dma.h>
#include <stm32/gpio.h>
#include <stm32/usart.h>

void com_init() {
  // Init serial console.
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO_USART1_RX);
  usart_set_baudrate(USART1, 72000000, 921600);
  usart_set_mode(USART1, USART_MODE_TX_RX);
  usart_set_databits(USART1, 9);
  usart_set_stopbits(USART1, USART_STOPBITS_1);
  usart_set_parity(USART1, USART_PARITY_EVEN);
  usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
  // Enable interrupt on uart receive.
  nvic_set_correct_priority(NVIC_USART1_IRQ, 10);
  nvic_enable_irq(NVIC_USART1_IRQ);
  usart_enable_rx_interrupt(USART1);
  // USART1_TX on DMA channel 4.
  nvic_set_correct_priority(NVIC_DMA1_CHANNEL4_IRQ, 3);
  nvic_enable_irq(NVIC_DMA1_CHANNEL4_IRQ);
  dma_channel_reset(DMA1, DMA_CHANNEL4);
  dma_set_peripheral_address(DMA1, DMA_CHANNEL4, (uintptr_t)&USART1_DR);
  dma_set_read_from_memory(DMA1, DMA_CHANNEL4);
  dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL4);
  dma_set_peripheral_size(DMA1, DMA_CHANNEL4, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA1, DMA_CHANNEL4, DMA_CCR_MSIZE_8BIT);
  dma_set_priority(DMA1, DMA_CHANNEL4, DMA_CCR_PL_MEDIUM);
  dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL4);
  dma_enable_transfer_error_interrupt(DMA1, DMA_CHANNEL4);
  // Activate.
  usart_enable(USART1);
}

void com_deinit() {
  usart_disable_rx_interrupt(USART1);
  usart_disable_tx_dma(USART1);
  dma_disable_channel(DMA1, DMA_CHANNEL4);
  dma_channel_reset(DMA1, DMA_CHANNEL4);
  usart_disable(USART1);
  nvic_disable_irq(NVIC_DMA1_CHANNEL4_IRQ);
  nvic_disable_irq(NVIC_USART1_IRQ);
}