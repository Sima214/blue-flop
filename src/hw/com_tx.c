#include "com.h"

#include <cm3ex.h>
#include <codecs.h>
#include <control.h>
#include <macros.h>
#include <stats.h>

#include <stm32/dma.h>
#include <stm32/usart.h>

#include <stddef.h>
#include <stdint.h>
#include <string.h>

typedef char com_state_t;

#define COM_BUFFER_SIZE 128U

/**
 * Track last buffer contents.
 */
static com_state_t com_state = COM_ID_NUL;
static uint8_t com_buffer1[COM_BUFFER_SIZE];
static uint8_t com_buffer2[COM_BUFFER_SIZE];
static const uint8_t* com_front = com_buffer1;
static size_t com_front_len;
static uint8_t* com_back = com_buffer2;
static size_t com_back_pos = 0;
static int volatile com_tx_complete = 1;

interrupt void dma1_channel4_isr() {
  if(hot_branch(dma_get_interrupt_flag(DMA1, DMA_CHANNEL4, DMA_TCIF))) {
    dma_clear_interrupt_flags(DMA1, DMA_CHANNEL4, DMA_TCIF);
    usart_disable_tx_dma(USART1);
    dma_disable_channel(DMA1, DMA_CHANNEL4);
    com_tx_complete = 1;
    stats.com.sent_bytes += com_front_len;
    stats.com.sent_packets++;
  }
  if(cold_branch(dma_get_interrupt_flag(DMA1, DMA_CHANNEL4, DMA_TEIF))) {
    dma_clear_interrupt_flags(DMA1, DMA_CHANNEL4, DMA_TEIF);
    usart_disable_tx_dma(USART1);
    dma_disable_channel(DMA1, DMA_CHANNEL4);
    com_tx_complete = 1;
    stats.com.error_dma++;
  }
}

static void com_swap_buffers() {
  // Wait for previous transfer to complete.
  while(!com_tx_complete) {
    // Do not take up all the bus bandwidth.
    nop;
  }
  // Prepare transfer.
  dma_set_memory_address(DMA1, DMA_CHANNEL4, (uintptr_t)com_back);
  dma_set_number_of_data(DMA1, DMA_CHANNEL4, com_back_pos);
  // Swap buffers.
  com_front_len = com_back_pos;
  uint8_t* tmp_back_buffer = com_back;
  com_back = (uint8_t*)com_front;
  com_front = tmp_back_buffer;
  com_back_pos = 0;
  // Start transfer.
  com_tx_complete = 0;
  dma_enable_channel(DMA1, DMA_CHANNEL4);
  usart_enable_tx_dma(USART1);
}

/**
 * The requested space must never be larger than the buffers' total size.
 * If it returns non zero, the transfer must not happen.
 */
static int com_ensure_space(com_error_t* err, com_flags_t flags, size_t space) {
  int got_enough = (com_back_pos + space) <= COM_BUFFER_SIZE;
  if(!got_enough) {
    *err |= COM_STATUS_OVF;
    // We need to swap buffers.
    if(mask_test(flags, COM_FLAG_NO_SWAP)) {
      // But we are not allowed to.
      return 1;
    } else if(mask_test(flags, COM_FLAG_NO_BLOCK) && !com_tx_complete) {
      // But we are not allowed to.
      *err |= COM_STATUS_WOULD_BLOCK;
      return 1;
    } else {
      com_swap_buffers();
    }
  }
  return 0;
}

static void com_correct_invalid_state(com_error_t* err, com_flags_t flags) {
  switch(com_state) {
    case COM_ID_NUL:
      // Not much we can do about that.
      break;
    case COM_ID_MSG:
      // Add null byte.
      if(com_ensure_space(err, flags, 1) == 0) {
        com_back[com_back_pos++] = '\0';
      } else {
        // If we cannot have one byte, then none else can have any byte.
        return;
      }
      break;
    case COM_ID_UPD:
      // Must not be 'atomic'.
      break;
    default:
      break;
  }
  // Hopefully nothing weird happens.
  com_state = COM_ID_NUL;
}

/**
 * If it returns zero, the transfer must not happen.
 * If it returns -1, state was reset.
 * If it returns 1 state was correct.
 */
static int com_check_invalid_state(com_error_t* err, com_flags_t flags, com_state_t req) {
  if(com_state != req) {
    *err |= COM_STATUS_INVALID_STATE;
    if(mask_test(flags, COM_FLAG_NOP_INVALID)) {
      return 0;
    } else {
      com_correct_invalid_state(err, flags);
      return -1;
    }
  }
  return 1;
}

/**
 * Public API.
 */
com_error_t com_msg_start(com_flags_t flags) {
  com_error_t ret = COM_STATUS_OK;
  if(com_check_invalid_state(&ret, flags, COM_ID_NUL) == 0) {
    return ret;
  }
  if(com_ensure_space(&ret, flags, 1) == 0) {
    com_back[com_back_pos++] = COM_ID_MSG;
    com_state = COM_ID_MSG;
  }
  return ret;
}

com_error_t com_msg_string(com_flags_t flags, const char* str) {
  com_error_t ret = COM_STATUS_OK;
  int state = com_check_invalid_state(&ret, flags, COM_ID_MSG);
  if(state == 0) {
    return ret;
  } else if(state == -1) {
    ret |= com_msg_start(flags);
  }
  size_t remain = strlen(str);
  while(remain != 0) {
    size_t block_size = min(max(COM_BUFFER_SIZE - com_back_pos, min(remain, 16U)), remain);
    if(com_ensure_space(&ret, flags, block_size)) {
      break;
    }
    // Space is ensured.
    memcpy(&com_back[com_back_pos], str, block_size);
    str += block_size;
    com_back_pos += block_size;
    remain -= block_size;
  }
  return ret;
}

#define max_length 10
com_error_t com_msg_i32(com_flags_t flags, int32_t v, int padding) {
  com_error_t ret = COM_STATUS_OK;
  int state = com_check_invalid_state(&ret, flags, COM_ID_MSG);
  if(state == 0) {
    return ret;
  } else if(state == -1) {
    ret |= com_msg_start(flags);
  }
  if(v < 0) {
    v = -v;
    if(com_ensure_space(&ret, flags, 1)) {
      return ret;
    } else {
      com_back[com_back_pos++] = '-';
    }
  }
  char s[max_length] = {0};
  for(int i = max_length - 1; i >= 0; i--) {
    int digit = v % 10;
    v = v / 10;
    s[i] = digit + 48;
  }
  int i;
  for(i = 0; i < max_length - padding; i++) {
    if(s[i] != '0') {
      break;
    }
  }
  size_t transfer_size = max_length - i;
  if(com_ensure_space(&ret, flags, transfer_size)) {
    return ret;
  }
  for(/**/; i < max_length; i++) {
    com_back[com_back_pos++] = s[i];
  }
  return ret;
}
#undef max_length

#define max_length 10
com_error_t com_msg_u32(com_flags_t flags, uint32_t v, int padding) {
  com_error_t ret = COM_STATUS_OK;
  int state = com_check_invalid_state(&ret, flags, COM_ID_MSG);
  if(state == 0) {
    return ret;
  } else if(state == -1) {
    ret |= com_msg_start(flags);
  }
  char s[max_length] = {0};
  for(int i = max_length - 1; i >= 0; i--) {
    int digit = v % 10;
    v = v / 10;
    s[i] = digit + 48;
  }
  int i;
  for(i = 0; i < max_length - padding; i++) {
    if(s[i] != '0') {
      break;
    }
  }
  size_t transfer_size = max_length - i;
  if(com_ensure_space(&ret, flags, transfer_size)) {
    return ret;
  }
  for(/**/; i < max_length; i++) {
    com_back[com_back_pos++] = s[i];
  }
  return ret;
}
#undef max_length

com_error_t com_msg_end(com_flags_t flags) {
  com_error_t ret = COM_STATUS_OK;
  if(com_check_invalid_state(&ret, flags, COM_ID_MSG) == 1) {
    if(com_ensure_space(&ret, flags, 1) == 0) {
      com_back[com_back_pos++] = '\0';
      com_state = COM_ID_NUL;
    }
  }
  return ret;
}

com_error_t com_ack(com_flags_t flags, int8_t status) {
  com_error_t ret = COM_STATUS_OK;
  if(com_check_invalid_state(&ret, flags, COM_ID_NUL) == 0) {
    return ret;
  }
  if(com_ensure_space(&ret, flags, 2) == 0) {
    com_back[com_back_pos++] = COM_ID_ACK;
    com_back[com_back_pos++] = status;
    com_state = COM_ID_NUL;
  }
  return ret;
}

com_error_t com_bulk(com_flags_t flags, int crc, int comp, const char* id, const void* data, size_t n) {
  com_error_t ret = COM_STATUS_OK;
  if(com_check_invalid_state(&ret, flags, COM_ID_NUL) == 0) {
    return ret;
  }
  size_t id_len = strlen(id) & 0b11111;
  // Standard header.
  if(com_ensure_space(&ret, flags, 2 + sizeof(n)) == 0) {
    com_back[com_back_pos++] = COM_ID_BLK;
    const uint8_t crc_mask = mask_create(2);
    const uint8_t comp_mask = mask_create(0) | mask_create(1);
    /**
     * 4 compression levels:
     *  0: No compression.
     *  1: Run Length Compression.
     *  2: Huffman.
     *  3: Some form of LZ(undecided).
     */
    uint8_t header = (comp & comp_mask) | (crc ? crc_mask : 0);
    header |= id_len << 3;
    com_back[com_back_pos++] = header;
    __builtin_memcpy(&com_back[com_back_pos], &n, sizeof(n));
    com_back_pos += sizeof(n);
  } else {
    return ret;
  }
  // ID header.
  if(com_ensure_space(&ret, flags, id_len) == 0) {
    memcpy(&com_back[com_back_pos], id, id_len);
    com_back_pos += id_len;
  } else {
    return ret;
  }
  ret |= com_flush(flags);
  if(ret != COM_STATUS_OK) {
    return ret;
  }
  if(!com_wait_ack()) {
    return ret | COM_STATUS_COMPOUND_DENIED;
  }
  // Data packets.
  const uint8_t* du8 = data;
  size_t remain = n;
  while(remain > 0) {
    size_t block_size = min(max(COM_BUFFER_SIZE - com_back_pos - (crc ? 8 : 4), min(remain, 16U)), remain);
    if(com_ensure_space(&ret, flags, sizeof(size_t) + block_size + (crc ? sizeof(uint32_t) : 0))) {
      break;
    }
    // Block size header.
    com_back[com_back_pos++] = COM_ID_BLK;
    __builtin_memcpy(&com_back[com_back_pos], &block_size, 3);
    com_back_pos += 3;
    // CRC optional header.
    if(crc) {
      uint32_t check = calc_crc(du8, block_size);
      __builtin_memcpy(&com_back[com_back_pos], &check, sizeof(uint32_t));
      com_back_pos += sizeof(uint32_t);
    }
    // Main data.
    memcpy(&com_back[com_back_pos], du8, block_size);
    com_back_pos += block_size;
    // Send to master.
    ret |= com_flush(flags);
    if(ret != COM_STATUS_OK) {
      return ret;
    }
    // Wait for master to send confirmation.
    if(com_wait_ack()) {
      // Transfer ok, move to next one.
      remain -= block_size;
      du8 += block_size;
    } else {
      // Retransmit.
    }
  }
  return ret;
}

com_error_t com_echo(com_flags_t flags, int8_t id, const void* data, uint16_t n) {
  com_error_t ret = COM_STATUS_OK;
  if(com_check_invalid_state(&ret, flags, COM_ID_NUL) == 0) {
    return ret;
  }
  if(com_ensure_space(&ret, flags, 1 + sizeof(id) + sizeof(n) + n) == 0) {
    com_state = COM_ID_ECH;
    com_back[com_back_pos++] = COM_ID_ECH;
    com_back[com_back_pos++] = id;
    com_back[com_back_pos++] = n & 0xff;
    com_back[com_back_pos++] = (n >> __CHAR_BIT__) && 0xff;
    memcpy(com_back, data, n);
    com_back_pos += n;
    com_state = COM_ID_NUL;
  }
  return ret;
}

com_error_t com_exit(com_flags_t flags) {
  com_error_t ret = COM_STATUS_OK;
  if(com_check_invalid_state(&ret, flags, COM_ID_NUL) == 0) {
    return ret;
  }
  if(com_ensure_space(&ret, flags, 1) == 0) {
    com_back[com_back_pos++] = COM_ID_EXI;
  }
  return ret;
}

com_error_t com_update_status(com_flags_t flags);

com_error_t com_update_append(com_flags_t flags, const control_packet_t* packet);

com_error_t com_update_bulk(const control_packet_t* packet) {
  if(packet != NULL) {
    return com_bulk(COM_FLAGS_DEFAULT, 1, 0, "control:registry", &packet->type, packet->length);
  } else {
    return com_ack(COM_FLAGS_DEFAULT, 0);
  }
}

com_error_t com_flush(com_flags_t flags) {
  com_error_t ret = COM_STATUS_OK;
  if(com_back_pos != 0) {
    if(mask_test(flags, COM_FLAG_NO_BLOCK) && !com_tx_complete) {
      ret |= COM_STATUS_WOULD_BLOCK;
    } else {
      com_swap_buffers();
    }
  }
  return ret;
}