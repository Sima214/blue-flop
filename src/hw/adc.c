#include "adc.h"

#include <stm32/adc.h>

void adc_init() {
  adc_power_off(ADC1);
  adc_power_off(ADC2);
  adc_enable_temperature_sensor();
  adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_28DOT5CYC);
  adc_power_on(ADC1);
  adc_power_on(ADC2);
}

void adc_deinit() {}