#ifndef BLFL_POWER
#define BLFL_POWER

#include <stdint.h>

/**
 * By default the core runs off the internal 8Mhz osc.
 * This switches the source to the external osc,
 * multiplied by PLL to 72Mhz.
 * 
 * Also all peripherals are set to their optimal frequency here.
 */
void power_clockup();

/**
 * Resets all enabled peripherals.
 * Usefull during abort() like situations.
 */
void power_panic_reset();

/**
 * Lights up the led used as a status indicator.
 * 
 * By default the led is on when the device is not
 * in sleep mode, and blinks when an unhandled exception occurs.
 */
void status_led_init();

void status_led_on();
void status_led_off();

/**
 * Makes the status led blink.
 * 
 * slow: pass non zero value if you want the led to blink slowly.
 * count: how many times to blink.
 */
void status_led_blink(int slow, uint32_t count);

/**
 * Stop main processing, but continue serving interrupts.
 * 
 * @returns the reason provided for the wakeup.
 */
int power_sleep();

/**
 * Called from an interrupt to resume execution.
 */
void power_wakeup(int reason);

#endif /*BLFL_POWER*/