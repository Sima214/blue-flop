#ifndef BLFL_CONTROL
#define BLFL_CONTROL

#include <macros.h>

#include <stddef.h>
#include <stdint.h>

// Forward declarations.
typedef struct control_state_t control_state_t;
typedef struct control_packet_t control_packet_t;
typedef struct control_registry_t control_registry_t;

/**
 * Return non zero on error.
 */
typedef int (*control_init_f)(const control_registry_t* self);

/**
 * Return non zero on error.
 */
typedef int (*control_update_direct_f)(const control_registry_t* self, const control_packet_t* update);

/**
 * Return non zero on error.
 */
typedef int (*control_update_async_f)(const control_registry_t* self);

/**
 * Return non zero on error.
 */
typedef int (*control_deinit_f)(const control_registry_t* self);

typedef enum {
  CONTROL_INVALID,
  CONTROL_DISPLAY_STRING,
  CONTROL_DISPLAY_INTEGER,
  CONTROL_DISPLAY_DECIMAL,
  CONTROL_DISPLAY_TIME,
  CONTROL_DISPLAY_TEMP,
  CONTROL_EDIT_BUTTON,
  CONTROL_EDIT_SLIDER,
  CONTROL_EDIT_COLOR,
  CONTROL_EDIT_MULTISELECT,
  CONTROL_MAX = 0x7fff
} ControlType;

typedef struct {
  char value[32];
} pack display_string_t;

typedef struct {
  int64_t value;
} pack display_integer_t;

typedef struct {
  int64_t value_integer;
  int64_t value_decimal;
} pack display_decimal_t;

typedef struct {
  int64_t unix_milliseconds;
} pack display_time_t;

typedef struct {
  int16_t temperature_major;
  int16_t temperature_minor;
  int16_t humidity_major;
  int16_t humidity_minor;
  int contains_humidity : 1;
  int contains_temp : 1;
  /** If neither, then temperature is in Kelvin. */
  int temp_celsius : 1;
  int temp_fahrenheit : 1;
} pack display_temp_t;

typedef struct {
  int state : 1;
  /** Is input allowed? */
  int active : 1;
  /** Is it a push button(0) or a toogle switch(1)? */
  int type : 1;
} pack edit_button_t;

typedef struct {
  int32_t value;
  int32_t active;
  int32_t min_value;
  int32_t max_value;
  int32_t step_size;
  int32_t display_float;
  int32_t display_float_offset;
  int32_t display_float_div;
} pack edit_slider_t;

typedef struct {
  unsigned int red : 8;
  unsigned int green : 8;
  unsigned int blue : 8;
  unsigned int intensity : 8;
  unsigned int active : 8;
} pack edit_color_t;

typedef struct {
  int32_t selected;
  int32_t active;
  int32_t count;
} pack edit_multiselect_t;

struct control_state_t {
  union {
    display_string_t disp_string;
    display_integer_t disp_integer;
    display_decimal_t disp_decimal;
    display_time_t disp_time;
    display_temp_t disp_temp;
    edit_button_t button;
    edit_slider_t slider;
    edit_color_t color;
    edit_multiselect_t select;
  };
};

struct control_packet_t {
  /** How many bytes this structure actually contains after this. */
  size_t length;
  /** What this structure contains. */
  ControlType type;
  /** Unique identifier. */
  uint16_t uid;
  /** Current state. */
  control_state_t state;
};

struct control_registry_t {
  /** What this structure contains. */
  ControlType type;
  /** Unique identifier. */
  uint16_t uid;
  /** Current state. */
  control_state_t* state;
  /** Callbacks */
  control_init_f init_callback;
  /** Gets called inside an interrupt. */
  control_update_direct_f update_callback;
  /** Gets called inside thread mode. */
  control_update_async_f thread_callback;
  control_deinit_f deinit_callback;
};

const control_registry_t* get_control_registry();

size_t get_control_registry_count();

size_t get_state_size_from_type(ControlType);

/**
 * Constructs a full control update/init packet.
 */
#define CONTROL_PACKET_CONSTRUCT(in_type, in_uid, in_pstate)                     \
  ({                                                                             \
    ControlType _t = (in_type);                                                  \
    int16_t _u = (in_uid);                                                       \
    size_t _l = get_state_size_from_type(_t);                                    \
    control_packet_t _control_tmp = (control_packet_t){_l, _t, _u, {}};          \
    _control_tmp.length += sizeof(_control_tmp.type) + sizeof(_control_tmp.uid); \
    memcpy(&_control_tmp.state, (in_pstate), _l);                                \
    _control_tmp;                                                                \
  })

#endif /*BLFL_CONTROL*/