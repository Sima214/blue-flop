from master.const import SPLASH_UI_RESOURCE_PATH
from master.communicator import ComBulkListener, Communicator
from master.utils import glib_mainloop_process_pending
from master.controller import Controller
import master.controls as control

import traceback
import signal
import ubjson
import lzma
import gi

gi.require_version('Gtk', '3.0')

# Gi special imports.
from gi.repository import Gtk, GLib


def mainloop_update_splash(builder, state, substate, progress):
    builder.get_object("splash_state").set_text(state)
    builder.get_object("splash_substate").set_text(substate)
    builder.get_object("splash_progress").set_fraction(progress)


def idle_add_update_splash(builder, state, substate, progress):
    def _idle_receiver(m_arg):
        mainloop_update_splash(m_arg[0], m_arg[1], m_arg[2], m_arg[3])
    g_arg = (builder, state, substate, progress)
    GLib.idle_add(_idle_receiver, g_arg)


class ControlRegistryListener(ComBulkListener):
    def __init__(self, com: Communicator, handler, builder, controller: Controller):
        super().__init__(com=com, regex=r'control:registry')
        self.builder = builder
        self.controller = controller
        self.registry_len = None

    def on_end_received(self, code):
        if code == 0:
            idle_add_update_splash(
                self.builder, "Syncing Controls...",
                "All of controls received!", 0.6
            )
            self.remove_listener()
        else:
            print("Was expecting ACK(0) but got ACK(%i)!" % (code))
            return True

    def on_part_received(self, valid, part):
        if valid and len(self.data) >= len(control.ControlPacket):
            header = control.ControlPacket.decode_header(self.data)
            header_index = header.uid
            # Find Registry size.
            if self.registry_len is None:
                self.com.add_ack_listener(self.on_end_received)
                # First received is the last one.
                self.registry_len = header_index + 1
            # Report progress.
            if self.registry_len is not None:
                part_full_progress = 1 / self.registry_len
                header_rindex = self.registry_len - 1 - header_index
                part_progress = len(self.data) / self.length
                total_progress = (header_rindex + part_progress) * part_full_progress
                idle_add_update_splash(
                    self.builder, "Syncing Controls...",
                    "Receiving %i/%i controls..." % (header_rindex, self.registry_len),
                    0.1 + total_progress * 0.5
                )

    def on_transfer_complete(self):
        rcontrol = control.parse_update_packet(self.com, self.data)
        if rcontrol is not None:
            self.controller.add_control(rcontrol)


class ControlDisplayListener(ComBulkListener):
    def __init__(self, com: Communicator, handler, builder, controller: Controller):
        super().__init__(com=com, regex=r'control:display')
        self.builder = builder
        self.controller = controller
        self.handler = handler

    def on_part_received(self, valid, part):
        if valid:
            progress = len(self.data) / self.length
            idle_add_update_splash(
                self.builder, "Syncing Visuals...",
                "Received %.2fKiB/%.2fKiB" % (len(self.data) / 1024, self.length / 1024),
                0.6 + progress * 0.4
            )

    def on_transfer_complete(self):
        decomp_data = lzma.decompress(self.data, format=lzma.FORMAT_RAW,
                                      filters=[{"id": lzma.FILTER_LZMA2, "preset": 9}])
        extra_data = ubjson.loadb(decomp_data)
        if "controls" in extra_data:
            for uid, obj in enumerate(extra_data["controls"]):
                self.controller.add_control_extra_data(uid, obj)
        if "categories" in extra_data:
            for obj in extra_data["categories"]:
                cat = control.Category.construct_from_obj(obj)
                self.controller.add_category(cat)
        self.remove_listener()
        self.handler._on_exit(self.com)


class SplashHandler:
    def __init__(self, builder, com: Communicator, controller: Controller):
        self.builder = builder
        self.com = com
        self.controller = controller
        self.done = False
        GLib.idle_add(self.com_init_phase1)
        com.add_exit_listener(self._on_exit)

    def on_exit(self, *args):
        if not self.done:
            self.com.close()

    def _on_exit(self, com: Communicator):
        # This must be triggered only once.
        try:
            com.listeners_exit.remove(self._on_exit)
        except ValueError as _:
            traceback.print_exc()
            print("Could not remove handshaker's exit handler!")
        GLib.idle_add(self._on_exit_mainloop)

    def _on_exit_mainloop(self):
        # Close window.
        self.builder.get_object("splash_window").close()
        self.done = True
        Gtk.main_quit()

    def com_init_phase2(self, value):
        "Called when remote responds on the sync command."
        if value == 0:
            print("Sync failed! Retrying...")
            self.com.send_sync()
            return True
        elif value == 1:
            idle_add_update_splash(self.builder, "Syncing...",
                                   "Receiving configuration...", 0.1)
            return False
        else:
            print("Unexpected ACK(%i)" % (value))
            self.on_exit()

    def com_init_phase1(self):
        com = self.com
        if com.ready:
            mainloop_update_splash(self.builder, "Syncing...", "Sending initial state...", 0.05)
            # Register listeners for next steps now to avoid race conditions.
            com.add_bulk_listener(ControlRegistryListener(com, self, self.builder, self.controller))
            com.add_bulk_listener(ControlDisplayListener(com, self, self.builder, self.controller))
            # Perform initial sync.
            com.add_ack_listener(self.com_init_phase2)
            com.send_sync()
        else:
            GLib.idle_add(self.com_init_phase1, com)


def main_handshaker(com: Communicator):
    splash = Gtk.Builder()
    splash.add_from_file(SPLASH_UI_RESOURCE_PATH)
    # Connect Handler.
    controller = Controller(com)
    handler = SplashHandler(splash, com, controller)
    splash.connect_signals(handler)
    # Dynamic ui building.
    splash.get_object("splash_state").set_text("Initializing Connection...")
    splash.get_object("splash_substate").set_text("")
    # Show window.
    splash_window = splash.get_object("splash_window")
    splash_window.show_all()
    # UI loop.
    signal.signal(signal.SIGINT, handler.on_exit)
    Gtk.main()
    glib_mainloop_process_pending()
    # Clean remaining handlers.
    signal.signal(signal.SIGINT, signal.default_int_handler)
    # Move on.
    if com.ready:
        controller.main()
    else:
        print("Connection closed while syncing.")
