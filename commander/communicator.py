from master.const import COM_RECORD_TX_POSTFIX, COM_RECORD_RX_POSTFIX
from master.utils import show_notification, STM32_CRC

import threading
import traceback
import cstruct
import select
import serial
import enum
import time
import math
import os
import re

ID_ACK = b'A'
ID_BLK = b'B'
ID_ECH = b'E'
ID_ERS = b'R'
ID_EXI = b'X'
ID_MSG = b'M'
ID_STA = b'S'
ID_UPD = b'U'


class ComStructSync(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        uint32_t timestamp_seconds;
        int32_t timestamp_microsec;
        uint32_t timestamp_xor;
    """

    def __init__(self, sec, usec):
        super().__init__(None)
        self.timestamp_seconds = sec
        self.timestamp_microsec = usec
        self.timestamp_xor = sec ^ usec


class ComBulkListener():

    class CompressionLevels(enum.IntEnum):
        NONE = 0
        RLC = 1
        HUFFMAN = 2
        LZ = 3

    def __init__(self, com, regex=None):
        self.com = com
        self.regex = regex
        self.data = bytearray()

    def on_header(self):
        "Return true to accept the transfer."
        if self.regex is not None:
            return re.match(self.regex, self.id) is not None
        else:
            return False

    def on_part_received(self, valid, part):
        "Called on each part of the data transfer."
        pass

    def on_transfer_complete(self):
        "Called when all the data has been received."
        pass

    def remove_listener(self):
        "Removes itself from the listeners list."
        self.com.listeners_bulk.remove(self)

    def _consume_part(self, buffer: bytearray):
        "Returns False when there is not enough data yet."
        header_size = 8 if self.crc else 4
        if len(buffer) < header_size:
            return False
        block_size = int.from_bytes(buffer[1:4], byteorder='little')
        if self.crc:
            self.crc_code = int.from_bytes(buffer[4:8], byteorder='little')
        block = buffer[header_size:block_size + header_size]
        valid = True
        # Process block - CRC.
        calc_crc = STM32_CRC(block)
        if calc_crc != self.crc_code:
            valid = False
            print("Invalid CRC code: expected `%x`, got `%x`." % (calc_crc, self.crc_code))
        # Process block - TODO: decompress.
        # Update buffers.
        if valid:
            self.com.send_ack(1)
            self.data.extend(block)
        else:
            self.com.send_ack(0)
        del buffer[:header_size + block_size]
        # `Fire` events.
        self.on_part_received(valid, block)
        if len(self.data) == self.length:
            self.on_transfer_complete()
            # Reset state.
            self.com.read_bulk_state = None
        return True

    def _consume_header(self, buffer: bytearray):
        "Returns count of bytes consumed. Does NOT actually delete bytes."
        if len(buffer) < 6:
            return 0
        flags = buffer[1]
        self.crc = (flags & 0b100) != 0
        self.compression = ComBulkListener.CompressionLevels(flags & 0b11)
        self.length = int.from_bytes(buffer[2:6], byteorder='little')
        id_len = flags >> 3
        if len(buffer) < id_len + 6:
            return 0
        self.id = buffer[6:id_len + 6].decode("ascii")
        self.data.clear()
        return id_len + 6


class Communicator:
    """Serial communications handler."""

    def __init__(self, options):
        # Construct.
        self.ready = False
        self.read_buffer = bytearray()
        self.read_bulk_state = None
        self.listeners_exit = []
        self.listeners_ack = []
        self.listeners_bulk = []
        self.listeners_echo = []
        self.listeners_sync = []
        self.listeners_update = []
        self.listeners_message = []
        # Parse and prepare.
        serial_device = options.get("port_path", "")
        serial_baud = int(options.get("baud_rate", 9600))
        serial_stopbits = serial.STOPBITS_ONE
        if options["stop_bits"] == "1.5":
            serial_stopbits = serial.STOPBITS_ONE_POINT_FIVE
        elif options["stop_bits"] == "2":
            serial_stopbits = serial.STOPBITS_TWO
        elif options["stop_bits"] != "1":
            msg = "Unsupported string for stop bits: `%s`!" % (
                options["stop_bits"]
            )
            show_notification(
                "Communicator construction error!",
                msg, "dialog-error"
            )
        serial_parity = serial.PARITY_NONE
        if options["parity"] == "Even":
            serial_parity = serial.PARITY_EVEN
        elif options["parity"] == "Odd":
            serial_parity = serial.PARITY_ODD
        elif options["parity"] != "None":
            msg = "Unsupported string for parity bit: `%s`!" % (
                options["parity"])
            show_notification(
                "Communicator construction error!", msg, "dialog-error"
            )
        serial_databits = int(options.get("data_bits", 8))
        self.record_tx = None
        self.record_rx = None
        # Setup recording.
        if options.get("record", False):
            record_prefix = time.strftime(options.get("record_prefix", ""))
            record_tx_filepath = os.path.join(
                options.get("record_path", ""),
                record_prefix + COM_RECORD_TX_POSTFIX
            )
            record_rx_filepath = os.path.join(
                options.get("record_path", ""),
                record_prefix + COM_RECORD_RX_POSTFIX
            )
            self.record_tx = open(record_tx_filepath, 'wb')
            self.record_rx = open(record_rx_filepath, 'wb')
        # Create port.
        self.sport = serial.serial_for_url(
            serial_device,
            baudrate=serial_baud,
            bytesize=serial_databits,
            parity=serial_parity,
            stopbits=serial_stopbits
        )
        # Start reading thread.
        self.read_thread = threading.Thread(target=self.read_loop, name="Serial Read")
        while not self.sport.is_open:
            time.sleep(0)
        self.read_thread.start()

    def __read_consume_buffer(self):
        "Returns False when there is not enough data yet."
        idc = self.read_buffer[0:1]
        if idc == ID_ACK:
            if len(self.read_buffer) >= 2:
                code = self.read_buffer[1]
                self.listeners_ack = [
                    l for l in self.listeners_ack if l(code)
                ]
                del self.read_buffer[:2]
                return True
            else:
                return False
        elif idc == ID_BLK:
            if self.read_bulk_state is None:
                # Waiting for header.
                n = 0
                for l in self.listeners_bulk:
                    n = l._consume_header(self.read_buffer)
                    if n > 0:
                        # Header is valid.
                        if l.on_header():
                            self.read_bulk_state = l
                            del self.read_buffer[:n]
                            self.send_ack(1)
                            return True
                    else:
                        # Invalid header.
                        return False
                # Valid header, but no listener.
                del self.read_buffer[:n]
                self.send_ack(0)
                print("No listener for bulk transfer!")
                return True
            else:
                # Parts processing.
                if self.read_bulk_state._consume_part(self.read_buffer):
                    return True
        elif idc == ID_ECH:
            # TODO: Echo back.
            pass
        elif idc == ID_ERS:
            # TODO: Fire events.
            pass
        elif idc == ID_EXI:
            # Remote is probably already gone.
            print("Remote has requested exit!")
            self.ready = False
            self.close()
            return False
        elif idc == ID_MSG:
            msg_end_index = self.read_buffer.find(b'\0')
            if msg_end_index == -1:
                # End of string not found.
                return False
            msg = self.read_buffer[1:msg_end_index].decode("utf-8")
            del self.read_buffer[:msg_end_index + 1]
            print(msg)
            for l in self.listeners_message:
                l(msg)
            return True
        elif idc == ID_STA:
            # TODO: Fire events.
            pass
        elif idc == ID_UPD:
            length = int.from_bytes(self.read_buffer[1:5], byteorder='little')
            if len(self.read_buffer) >= 5 + length:
                packet = bytes(self.read_buffer[5:length + 5])
                del self.read_buffer[:length + 5]
                for l in self.listeners_update:
                    l(packet)
                return True
        else:
            self.read_buffer = bytearray()
            print("Unknown COM ID: `%c`. Trying to resyncronize.")
        return False

    def __read_data(self, data):
        # Append newly received data to buffer.
        self.read_buffer.extend(data)
        # Consume buffer.
        while len(self.read_buffer) > 0 and self.__read_consume_buffer():
            pass
        # Optional logging/recording is done in the end to minimize latency.
        if self.record_rx is not None:
            self.record_rx.write(data)

    def read_loop(self):
        poller = select.poll()
        sport_fd = self.sport.fileno()
        poller.register(sport_fd, select.POLLIN | select.POLLPRI)
        self.ready = True
        while self.ready:
            polled_list = poller.poll(100)
            if len(polled_list) == 1:
                polled_mask = polled_list[0][1]
                # Invalid programming.
                if polled_mask & select.POLLNVAL != 0:
                    if self.ready:
                        print("File descriptor got closed while polling!")
                        self.ready = False
                    break
                # File closed.
                if polled_mask & (select.POLLERR | select.POLLHUP) != 0:
                    print("Serial port errored while polling!")
                    self.ready = False
                    self.close()
                    break
                # Data to read.
                if polled_mask & (select.POLLIN | select.POLLPRI) != 0:
                    try:
                        newdata = self.sport.read_all()
                    except OSError as _:
                        traceback.print_exc()
                        self.ready = False
                        self.close()
                    if len(newdata) == 0:
                        print("OS said there was data to read, but there was nothing!")
                    else:
                        self.__read_data(newdata)
        poller.unregister(sport_fd)

    def send(self, data):
        self.sport.write(data)
        if self.record_tx is not None:
            self.record_tx.write(data)

    def send_exit(self):
        data = ID_EXI
        self.send(data)

    def send_ack(self, code):
        data = ID_ACK + code.to_bytes(length=1, byteorder='little')
        self.send(data)

    def send_echo(self, msg):
        data = ID_ECH + len(msg).to_bytes(1, byteorder='little') + msg
        self.send(data)

    def send_sync(self):
        timestamp_sec = time.time()
        timestamp_us = math.floor(timestamp_sec * 1000000)
        timestamp_sec = math.floor(timestamp_sec)
        timestamp_us = timestamp_us - timestamp_sec * 1000000
        packet = ComStructSync(timestamp_sec, timestamp_us)
        data = ID_STA + packet.pack()
        self.send(data)

    def send_update(self, packet: bytes):
        data = ID_UPD + len(packet).to_bytes(4, byteorder='little') + packet
        self.send(data)

    def add_exit_listener(self, listener):
        self.listeners_exit.append(listener)

    def add_ack_listener(self, listener):
        self.listeners_ack.append(listener)

    def add_bulk_listener(self, listener: ComBulkListener):
        self.listeners_bulk.append(listener)

    def add_sync_listener(self, listener):
        self.listeners_sync.append(listener)

    def add_update_listener(self, listener: ComBulkListener):
        self.listeners_update.append(listener)

    def add_msg_listener(self, listener):
        self.listeners_message.append(listener)

    def close(self):
        "Closes the serial port."
        if self.ready:
            self.send_exit()
            self.sport.close()
            self.ready = False
        listeners_exit_copy = self.listeners_exit.copy()
        for l in listeners_exit_copy:
            l(self)
