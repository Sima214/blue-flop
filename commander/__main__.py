#!/usr/bin/env python

# Execute with:
# python -m master

from master.selector import main_select

import sys
import os
import gi

gi.require_version("Notify", '0.7')

# Special imports.
from gi.repository import Notify


if __package__ is None and not hasattr(sys, 'frozen'):
    # direct call of __main__.py
    path = os.path.realpath(os.path.abspath(__file__))
    sys.path.insert(0, os.path.dirname(os.path.dirname(path)))

if __name__ == '__main__':
    Notify.init("blueflop")
    main_select()
    Notify.uninit()
