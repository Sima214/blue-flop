import os
from xdg import XDG_CONFIG_HOME

INSTALL_DIR = os.path.dirname(os.path.abspath(__file__))

BOOTLOADER_EXEC = "stm32flash"

UI_RESOURCE_PATH = os.path.join(INSTALL_DIR, "ui")

CONFIG_PATH = os.path.join(XDG_CONFIG_HOME, "blueflop")
if not os.path.exists(CONFIG_PATH):
    os.mkdir(CONFIG_PATH)

SELECTOR_CONFIG_FILE = os.path.join(CONFIG_PATH, "selector.json")

CSS_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "style.css")

SELECTOR_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "selector.glade")

SPLASH_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "loading_splash.glade")

CONTROLLER_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "controller.glade")

CATEGORY_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "category.glade")

CONTROL_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_base.glade")

DISP_STRING_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_disp_string.glade")

DISP_INTEGER_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_disp_integer.glade")

DISP_DECIMAL_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_disp_decimal.glade")

DISP_TIME_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_disp_time.glade")

DISP_TEMP_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_disp_temp.glade")

EDIT_BUTTON_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_edit_button.glade")

EDIT_SLIDER_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_edit_slider.glade")

EDIT_COLOR_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_edit_color.glade")

EDIT_MULTISELECT_UI_RESOURCE_PATH = os.path.join(UI_RESOURCE_PATH, "control_edit_multiselect.glade")

COM_RECORD_TX_POSTFIX = ".tx.bin"

COM_RECORD_RX_POSTFIX = ".rx.bin"
