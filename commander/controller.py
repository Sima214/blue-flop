from master.utils import glib_mainloop_process_pending
from master.const import CONTROLLER_UI_RESOURCE_PATH
from master.communicator import Communicator
import master.controls as control

import traceback
import signal
import gi

gi.require_version('Gtk', '3.0')

# Gi special imports.
from gi.repository import Gtk, GLib


class Controller:
    def __init__(self, com: Communicator):
        self.controls = []
        self.categories = []
        self.com = com

    def add_control(self, cnt: control.ControlBase):
        if cnt.uid + 1 > len(self.controls):
            padding = [None] * (cnt.uid - len(self.controls) + 1)
            self.controls.extend(padding)
        self.controls[cnt.uid] = cnt
        print("Registered: %s" % repr(cnt))

    def add_control_extra_data(self, uid, obj_ex):
        self.controls[uid].add_extra(obj_ex)

    def add_category(self, cat: control.Category):
        cat.resolve_references(self.controls)
        self.categories.append(cat)
        print("Registered: %s" % repr(cat))

    def _main_validate(self):
        if None in self.controls:
            raise RuntimeError("There is a hole in the controls list!")
        for v in self.controls:
            if not v.validate():
                raise RuntimeError("Remote state is not valid!")
        for v in self.categories:
            if not v.validate():
                raise RuntimeError("Remote state is not valid!")

    def _build_ui(self):
        root = Gtk.Builder()
        root.add_from_file(CONTROLLER_UI_RESOURCE_PATH)
        # Connect handlers.
        root.connect_signals(self)
        self.log_buffer = root.get_object("controller_log_buffer")
        self.categories_ui = root.get_object("controller_categories")
        # Dynamic UI.
        for c in reversed(self.categories):
            tab, title = c.build_ui()
            self.categories_ui.prepend_page(tab, title)
        return root

    def main(self):
        self._main_validate()
        # Build UI.
        self.ui_root = self._build_ui()
        # Show window.
        self.ui_root.get_object("controller_window").show_all()
        # Register self to receive events.
        signal.signal(signal.SIGINT, self.on_exit)
        self.com.add_exit_listener(self._on_exit)
        self.com.add_msg_listener(self.on_message)
        self.com.add_sync_listener(self.on_sync_packet)
        self.com.add_update_listener(self.on_update_packet)
        # Enter main loop.
        Gtk.main()
        # Clean up for exit.
        signal.signal(signal.SIGINT, signal.default_int_handler)
        glib_mainloop_process_pending()

    def _on_exit_mainloop(self):
        # Close window.
        self.ui_root.get_object("controller_window").close()
        Gtk.main_quit()

    def _on_exit(self, com: Communicator):
        # This must be triggered only once.
        try:
            com.listeners_exit.remove(self._on_exit)
        except ValueError as _:
            traceback.print_exc()
            print("Could not remove handshaker's exit handler!")
        GLib.idle_add(self._on_exit_mainloop)

    def on_exit(self, *args):
        if self.com.ready:
            self.com.send_exit()
        self.com.close()

    def _on_message(self, msg: str):
        pass

    def on_message(self, msg: str):
        GLib.idle_add(self._on_message, msg)

    def on_sync_packet(self, data: bytes):
        pass

    def on_update_packet(self, data: bytes):
        header = control.ControlPacket.decode_header(data)
        self.controls[header.uid].on_update(data)
