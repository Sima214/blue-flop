from master.const import SELECTOR_UI_RESOURCE_PATH, CSS_UI_RESOURCE_PATH, SELECTOR_CONFIG_FILE, BOOTLOADER_EXEC
from master.utils import show_notification
from master.handshaker import main_handshaker
from master.communicator import Communicator

from serial.tools.list_ports import comports as list_ports
import subprocess
import traceback
import signal
import time
import json
import sys
import os
import re
import gi

gi.require_version('Gtk', '3.0')

# Gi special imports.
from gi.repository import GObject, Gtk, Gdk, GLib

BOOTLOADER_MODEL_PATTERN = re.compile(r'Device ID\s*:\s*[x\d]*\s*\((.*)\)')
BOOTLOADER_FLASH_PATTERN = re.compile(r'Flash\s*:\s*([\w]*)\s*')
BOOTLOADER_RAM_PATTERN = re.compile(r'RAM\s*:\s*([\w]*)\s*')


class SelectorHandler:
    def __init__(self, builder):
        self.launch = False
        self.builder = builder

    def onExit(self, *args):
        if not self.launch:
            Gtk.main_quit()

    def updatePorts(self, *args):
        # Enumerate serial ports.
        ports = list_ports()
        # Get list store.
        port_store = self.builder.get_object(
            "selector_device_list").get_model()
        # Update list store.
        port_store.clear()
        for p in ports:
            port_store.append([p.device])

    def selectPort(self, port_list, *args):
        port_iter = port_list.get_active_iter()
        if port_iter is not None:
            chooser = self.builder.get_object("selector_file")
            port_store = port_list.get_model()
            chooser.set_filename(port_store[port_iter][0])

    def updateBootloaderMode(self, tbtn, *args):
        self.builder.get_object(
            "selector_test").set_sensitive(tbtn.get_active())

    def updateRecordMode(self, tbtn, *args):
        self.builder.get_object(
            "selector_com_record_prefix").set_sensitive(tbtn.get_active())
        self.builder.get_object(
            "selector_com_record_path").set_sensitive(tbtn.get_active())

    def testBootloader(self, *args):
        tty_path = self.builder.get_object("selector_file").get_filename()
        if tty_path is not None:
            # Glib route was kinda complicated and I already know how to do it the python way.
            proc = subprocess.Popen([BOOTLOADER_EXEC, str(
                tty_path)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            GLib.idle_add(self.testBootloader2, proc)

    def testBootloader2(self, proc):
        rid = proc.poll()
        if rid is None:
            # Do not block!
            GLib.idle_add(self.testBootloader2, proc)
        elif rid == 0:
            info = proc.stdout.read().decode("utf-8")
            info_model = BOOTLOADER_MODEL_PATTERN.search(info).group(1)
            info_flash = BOOTLOADER_FLASH_PATTERN.search(info).group(1)
            info_ram = BOOTLOADER_RAM_PATTERN.search(info).group(1)
            self.builder.get_object("selector_model").set_label(info_model)
            self.builder.get_object("selector_flash").set_label(info_flash)
            self.builder.get_object("selector_ram").set_label(info_ram)
        else:
            show_notification("Bootloader test failed!", proc.stdout.read().decode(
                "utf-8"), "dialog-warning-symbolic")

    def connect(self, *args):
        self.launch = True
        Gtk.main_quit()


def main_select():
    selector = Gtk.Builder()
    selector.add_from_file(SELECTOR_UI_RESOURCE_PATH)
    # Load style.
    css = Gtk.CssProvider()
    css.load_from_path(CSS_UI_RESOURCE_PATH)
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), css, Gtk.STYLE_PROVIDER_PRIORITY_USER)
    # Connect Handler.
    handler = SelectorHandler(selector)
    selector.connect_signals(handler)
    # Dynamic UI building.
    selector.get_object("selector_device_list").set_model(Gtk.ListStore(str))
    GLib.idle_add(handler.updatePorts)
    # Restore saved state (if any).
    state = None
    if os.path.exists(SELECTOR_CONFIG_FILE):
        with open(SELECTOR_CONFIG_FILE, 'r') as fc:
            state = json.load(fc)
    try:
        if state is not None:
            if state.get("port_path") is not None:
                selector.get_object("selector_file").set_filename(
                    str(state["port_path"]))
            if state.get("baud_rate") is not None:
                selector.get_object("selector_baud_entry").set_text(
                    str(state["baud_rate"]))
            if state.get("stop_bits") is not None:
                selector.get_object("selector_stop_bits").set_active_id(
                    str(state["stop_bits"]))
            if state.get("parity") is not None:
                selector.get_object("selector_parity").set_active_id(
                    str(state["parity"]))
            if state.get("data_bits") is not None:
                selector.get_object("selector_data_bits").set_value(
                    int(state["data_bits"]))
            if state.get("bootloader") is not None:
                selector.get_object("selector_uboot").set_active(
                    bool(state["bootloader"]))
            if state.get("record") is not None:
                selector.get_object("selector_com_record").set_active(
                    bool(state["record"]))
            if state.get("record_path") is not None:
                selector.get_object("selector_com_record_path").set_filename(
                    str(state["record_path"]))
            if state.get("record_prefix") is not None:
                selector.get_object("selector_com_record_prefix").set_text(
                    str(state["record_prefix"]))
    except Exception as e:
        # Report errors.
        print(e)
    # Show window.
    selector_window = selector.get_object("selector_window")
    selector_window.show_all()
    # UI loop.
    signal.signal(signal.SIGINT, Gtk.main_quit)
    Gtk.main()
    # Close selector.
    selector_window.close()
    # Save UI state.
    state = {}
    state["port_path"] = selector.get_object("selector_file").get_filename()
    state["baud_rate"] = selector.get_object("selector_baud").get_active_text()
    state["stop_bits"] = selector.get_object("selector_stop_bits").get_active_text()
    state["parity"] = selector.get_object("selector_parity").get_active_text()
    state["data_bits"] = selector.get_object("selector_data_bits").get_value_as_int()
    state["bootloader"] = selector.get_object("selector_uboot").get_active()
    state["record"] = selector.get_object("selector_com_record").get_active()
    state["record_path"] = selector.get_object("selector_com_record_path").get_filename()
    state["record_prefix"] = selector.get_object("selector_com_record_prefix").get_text()
    with open(SELECTOR_CONFIG_FILE, 'w') as fc:
        data = json.dumps(state)
        fc.write(data)
    # Move on.
    signal.signal(signal.SIGINT, signal.default_int_handler)
    if handler.launch:
        # Optional bootloader boot step.
        if state["bootloader"]:
            blrun_args = [BOOTLOADER_EXEC, "-g", "0x0", state["port_path"]]
            blrun_proc = subprocess.Popen(
                blrun_args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
            )
            blrun_retc = blrun_proc.wait()
            if blrun_retc == 0:
                blrun_notf = show_notification(
                    "Slave Initialization Delay", "", "view-refresh-symbolic"
                )
                time.sleep(0.03)
                blrun_notf.close()
            else:
                blrun_stdo = "%s\n%i" % (
                    blrun_proc.stdout.read().decode("utf-8"), blrun_retc
                )
                blrun_notf = show_notification(
                    "Slave Initialization Failed!", blrun_stdo, "dialog-error-symbolic"
                )
                sys.exit(1)
        # Handover to the other scripts.
        try:
            com = Communicator(state)
        except Exception as excpt:
            msg = traceback.format_exc()
            print(msg, file=sys.stderr)
            show_notification("Communicator initialization errored!",
                              msg, "dialog-error-symbolic")
            sys.exit(1)
        return main_handshaker(com)
