import crcmod
import gi

gi.require_version("Notify", '0.7')
gi.require_version('Gtk', '3.0')

# Gi special imports.
from gi.repository import GLib, Notify, Gtk

STM32_CRC = crcmod.mkCrcFun(0x104C11DB7, rev=True)


def show_notification(title: str, msg: str, icon_name: str = ""):
    n = Notify.Notification.new(title, msg, icon_name)
    n.show()
    return n

def glib_mainloop_process_pending():
    while Gtk.events_pending():
        Gtk.main_iteration_do(False)
