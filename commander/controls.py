from master.const import CATEGORY_UI_RESOURCE_PATH, CONTROL_UI_RESOURCE_PATH, DISP_STRING_UI_RESOURCE_PATH, DISP_INTEGER_UI_RESOURCE_PATH, DISP_DECIMAL_UI_RESOURCE_PATH, DISP_TIME_UI_RESOURCE_PATH, DISP_TEMP_UI_RESOURCE_PATH, EDIT_BUTTON_UI_RESOURCE_PATH, EDIT_SLIDER_UI_RESOURCE_PATH, EDIT_COLOR_UI_RESOURCE_PATH, EDIT_MULTISELECT_UI_RESOURCE_PATH
from master.communicator import Communicator

import cstruct
from datetime import datetime
import enum
import gi

gi.require_version('Gtk', '3.0')

# Gi special imports.
from gi.repository import Gtk, GLib


class ControlType(enum.IntEnum):
    CONTROL_DISPLAY_STRING = 1
    CONTROL_DISPLAY_INTEGER = 2
    CONTROL_DISPLAY_DECIMAL = 3
    CONTROL_DISPLAY_TIME = 4
    CONTROL_DISPLAY_TEMP = 5
    CONTROL_EDIT_BUTTON = 6
    CONTROL_EDIT_SLIDER = 7
    CONTROL_EDIT_COLOR = 8
    CONTROL_EDIT_MULTISELECT = 9


class ControlPacket(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        int16_t type;
        uint16_t uid;
    """

    @staticmethod
    def decode_header(data: bytes):
        ret = ControlPacket()
        ret.unpack(data[:len(ControlPacket())])
        return ret


class ControlPacketDisplayString(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        char value[32];
    """


class ControlPacketDisplayInteger(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        int64_t value;
    """


class ControlPacketDisplayDecimal(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        int64_t value_integer;
        int64_t value_decimal;
    """


class ControlPacketDisplayTime(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        int64_t unix_milliseconds;
    """


class ControlPacketDisplayTemp(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        int16_t temperature_major;
        int16_t temperature_minor;
        int16_t humidity_major;
        int16_t humidity_minor;
        int8_t flags;
    """
    CONTAINS_HUMIDITY = 0b0001
    CONTAINS_TEMP = 0b0010
    TEMP_CELSIUS = 0b0100
    TEMP_FAHRENHEIT = 0b1000

    @property
    def contains_humidity(self):
        return (self.flags & ControlPacketDisplayTemp.CONTAINS_HUMIDITY) != 0

    @contains_humidity.setter
    def contains_humidity(self, value: bool):
        v = ControlPacketDisplayTemp.CONTAINS_HUMIDITY if value else 0
        self.flags = (self.flags & ~ControlPacketDisplayTemp.CONTAINS_HUMIDITY) | v

    @property
    def contains_temperature(self):
        return (self.flags & ControlPacketDisplayTemp.CONTAINS_TEMP) != 0

    @contains_temperature.setter
    def contains_temperature(self, value: bool):
        v = ControlPacketDisplayTemp.CONTAINS_TEMP if value else 0
        self.flags = (self.flags & ~ControlPacketDisplayTemp.CONTAINS_TEMP) | v

    @property
    def temperature_type(self):
        celsius = (self.flags & ControlPacketDisplayTemp.TEMP_CELSIUS) != 0
        fahrenheit = (self.flags & ControlPacketDisplayTemp.TEMP_FAHRENHEIT) != 0
        if celsius:
            return 'C'
        elif fahrenheit:
            return 'F'
        else:
            return 'K'

    @temperature_type.setter
    def temperature_type(self, value: str):
        # Clear old values.
        self.flags = self.flags & ~(ControlPacketDisplayTemp.TEMP_CELSIUS |
                                    ControlPacketDisplayTemp.TEMP_FAHRENHEIT)
        if value == 'C':
            self.flags = self.flags | ControlPacketDisplayTemp.TEMP_CELSIUS
        elif value == 'F':
            self.flags = self.flags | ControlPacketDisplayTemp.TEMP_FAHRENHEIT
        elif value == 'K':
            pass
        else:
            raise ValueError("Argument value=`%s` is not any of (C, F, K)!" % (value))


class ControlPacketEditButton(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        int8_t flags;
    """
    STATE = 0b001
    ACTIVE = 0b010
    BUTTON_TYPE = 0b100

    @property
    def state(self):
        return self.flags & ControlPacketEditButton.STATE != 0

    @state.setter
    def state(self, value):
        v = ControlPacketEditButton.STATE if value else 0
        self.flags = (self.flags & ~ControlPacketEditButton.STATE) | v

    @property
    def active(self):
        return self.flags & ControlPacketEditButton.ACTIVE != 0

    @active.setter
    def active(self, value):
        v = ControlPacketEditButton.ACTIVE if value else 0
        self.flags = (self.flags & ~ControlPacketEditButton.ACTIVE) | v

    @property
    def button_type(self):
        return self.flags & ControlPacketEditButton.BUTTON_TYPE != 0

    @button_type.setter
    def button_type(self, value):
        v = ControlPacketEditButton.BUTTON_TYPE if value else 0
        self.flags = (self.flags & ~ControlPacketEditButton.BUTTON_TYPE) | v


class ControlPacketEditSlider(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        int32_t value;
        int32_t active;
        int32_t min_value;
        int32_t max_value;
        int32_t step_size;
        int32_t display_float;
        int32_t display_float_offset;
        int32_t display_float_div;
    """


class ControlPacketEditColor(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        uint8_t red;
        uint8_t green;
        uint8_t blue;
        uint8_t intensity;
        uint8_t active;
    """


class ControlPacketEditMultiselect(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        struct ControlPacket header;
        int32_t selected;
        int32_t active;
        int32_t count;
    """


class ControlBase:
    def __init__(self, com: Communicator, ctype, uid: int):
        self.categories = []
        self.com = com
        self.control_type = ControlType(ctype)
        self.uid = uid

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        raise RuntimeError("ControlBase.construct_from_data must be overridden!")

    def add_extra(self, obj: dict):
        self.title = obj.get("title", "")
        self.description = obj.get("desc", "")

    def add_category(self, cat):
        self.categories.append(cat)

    def validate(self):
        if len(self.categories) == 0:
            print("%s is not part of any category!" % repr(self))
            return False
        return True

    def build_ui(self):
        builder = Gtk.Builder()
        builder.add_from_file(CONTROL_UI_RESOURCE_PATH)
        builder.get_object("control_uid").set_label("%2i" % (self.uid))
        builder.get_object("control_title").set_label(self.title)
        builder.get_object("control_desc").set_label(self.description)
        specialization_root = builder.get_object("control_extra")
        local_root = builder.get_object("control_root")
        return local_root, specialization_root

    def on_update(self, data: bytes):
        raise RuntimeError("Unimplemented %s.on_update(%s)" % (type(self).__name__, str(data)))

    def __repr__(self):
        s = self.__class__.__name__ + '('
        for k, v in self.__dict__.items():
            if type(v) == ControlType:
                s = s + "%s=%i, " % (k, v)
            elif type(v) == bool or type(v) == int or type(v) == str:
                s = s + "%s=%s, " % (k, str(v))
        s = s[:-2] + ')'
        return s


class Category:
    def __init__(self, title: str = "", children: list = []):
        self.title = title
        self.children = children

    def resolve_references(self, controls: list):
        newlist = [None] * len(self.children)
        for i, uid in enumerate(self.children):
            c = controls[uid]
            c.add_category(self)
            newlist[i] = c
        self.children = newlist

    def validate(self):
        for v in self.children:
            if not issubclass(type(v), ControlBase):
                print("%s's children must all be of inherited type of ControlBase!")
                return False
        return True

    def build_ui(self):
        tab_builder = Gtk.Builder()
        tab_builder.add_from_file(CATEGORY_UI_RESOURCE_PATH)
        tab_contents = tab_builder.get_object("category_box")
        for child in self.children:
            child_ui = child.build_ui()
            tab_contents.pack_start(child_ui, False, True, 1)
            tab_contents.pack_start(Gtk.Separator(), False, True, 4)
        title = Gtk.Label(self.title)
        tab = tab_builder.get_object("category_root")
        return tab, title

    def __repr__(self):
        s = "%s(title=%s, children=[" % (self.__class__.__name__, self.title)
        for c in self.children:
            uid = -1
            if type(c) == int:
                uid = c
            elif issubclass(type(c), ControlBase):
                uid = c.uid
            s = s + str(uid) + ", "
        s = s[:-2] + '])'
        return s

    @staticmethod
    def construct_from_obj(obj: dict):
        t = obj.get("title", "")
        c = obj.get("children", [])
        return Category(t, c)


class ControlDisplayString(ControlBase):
    PACKET_CLASS = ControlPacketDisplayString

    def __init__(self, com, ctype, uid, string):
        super().__init__(com, ctype, uid)
        self.string = string

    def _update_value(self):
        "Must always run inside the mainloop."
        self.ui.set_label(self.string)

    def build_ui(self):
        root, extra = super().build_ui()
        builder = Gtk.Builder()
        builder.add_from_file(DISP_STRING_UI_RESOURCE_PATH)
        extra_root = builder.get_object("root")
        extra.add(extra_root)
        self.ui = builder.get_object("value")
        self._update_value()
        return root

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        padding = b'\0' * (cls.PACKET_CLASS.__size__ - len(data))
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        value = packet.value.decode("utf-8").rstrip('\0')
        ret = cls(com, ctyp, uid, value)
        return ret


class ControlDisplayInteger(ControlBase):
    PACKET_CLASS = ControlPacketDisplayInteger

    def __init__(self, com, ctype, uid, value):
        super().__init__(com, ctype, uid)
        self.value = value

    def _update_value(self):
        "Must always run inside the mainloop."
        self.ui.set_label(str(self.value))

    def build_ui(self):
        root, extra = super().build_ui()
        builder = Gtk.Builder()
        builder.add_from_file(DISP_INTEGER_UI_RESOURCE_PATH)
        extra_root = builder.get_object("root")
        extra.add(extra_root)
        self.ui = builder.get_object("value")
        self._update_value()
        return root

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        value = packet.value
        ret = cls(com, ctyp, uid, value)
        return ret


class ControlDisplayDecimal(ControlBase):
    PACKET_CLASS = ControlPacketDisplayDecimal

    def __init__(self, com, ctype, uid, value_major, value_minor):
        super().__init__(com, ctype, uid)
        self.value_major = value_major
        self.value_minor = value_minor

    def _update_value(self):
        "Must always run inside the mainloop."
        self.ui.set_label("%i.%i" % (self.value_major, self.value_minor))

    def build_ui(self):
        root, extra = super().build_ui()
        builder = Gtk.Builder()
        builder.add_from_file(DISP_DECIMAL_UI_RESOURCE_PATH)
        extra_root = builder.get_object("root")
        extra.add(extra_root)
        self.ui = builder.get_object("value")
        self._update_value()
        return root

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        val_maj = packet.value_integer
        val_min = packet.value_decimal
        ret = cls(com, ctyp, uid, val_maj, val_min)
        return ret


class ControlDisplayTime(ControlBase):
    PACKET_CLASS = ControlPacketDisplayTime

    def __init__(self, com, ctype, uid, timestamp):
        super().__init__(com, ctype, uid)
        self.timestamp = timestamp

    def _update_value(self):
        "Must always run inside the mainloop."
        dt = datetime.fromtimestamp(self.timestamp / 1000)
        self.ui_clock.set_label(dt.strftime("%H:%M:%S.%f"))
        self.ui_date.set_label(dt.strftime("%A %d %B %Y"))

    def build_ui(self):
        root, extra = super().build_ui()
        builder = Gtk.Builder()
        builder.add_from_file(DISP_TIME_UI_RESOURCE_PATH)
        extra_root = builder.get_object("root")
        extra.add(extra_root)
        self.ui_clock = builder.get_object("clock")
        self.ui_date = builder.get_object("date")
        self._update_value()
        return root

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        timestamp = packet.unix_milliseconds
        ret = cls(com, ctyp, uid, timestamp)
        return ret


class ControlDisplayTemp(ControlBase):
    PACKET_CLASS = ControlPacketDisplayTemp

    def __init__(self, com, ctype, uid,
                 temperature_major, temperature_minor,
                 humidity_major, humidity_minor,
                 contains_humidity, contains_temperature, temperature_type
                 ):
        super().__init__(com, ctype, uid)
        self.temperature_major = temperature_major
        self.temperature_minor = temperature_minor
        self.humidity_major = humidity_major
        self.humidity_minor = humidity_minor
        self.contains_humidity = contains_humidity
        self.contains_temperature = contains_temperature
        self.temperature_type = temperature_type

    def _update_value(self):
        "Must always run inside the mainloop."
        self.ui_temp.set_visible(self.contains_temperature)
        if self.contains_temperature:
            self.ui_temp.set_label(self.ui_temp_fmt % (
                self.temperature_major, self.temperature_minor, self.temperature_type))
        self.ui_humid.set_visible(self.contains_humidity)
        if self.contains_humidity:
            self.ui_humid.set_label(self.ui_humid_fmt % (
                self.humidity_major, self.humidity_minor))

    def build_ui(self):
        root, extra = super().build_ui()
        builder = Gtk.Builder()
        builder.add_from_file(DISP_TEMP_UI_RESOURCE_PATH)
        extra_root = builder.get_object("root")
        extra.add(extra_root)
        self.ui_humid = builder.get_object("humidity")
        self.ui_humid_fmt = self.ui_humid.get_label()
        self.ui_temp = builder.get_object("temperature")
        self.ui_temp_fmt = self.ui_temp.get_label()
        self._update_value()
        return root

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        temperature_major = packet.temperature_major
        temperature_minor = packet.temperature_minor
        humidity_major = packet.humidity_major
        humidity_minor = packet.humidity_minor
        contains_humidity = packet.contains_humidity
        contains_temperature = packet.contains_temperature
        temperature_type = packet.temperature_type
        ret = cls(com, ctyp, uid,
                  temperature_major, temperature_minor,
                  humidity_major, humidity_minor,
                  contains_humidity, contains_temperature, temperature_type
                  )
        return ret


class ControlEditButton(ControlBase):
    PACKET_CLASS = ControlPacketEditButton

    def __init__(self, com, ctype, uid, state, active: bool, button_type):
        super().__init__(com, ctype, uid)
        self.state = state
        self.active = active
        self.button_type_toggle = button_type

    def _update_value(self):
        "Must always run inside the mainloop."
        if self.button_type_toggle:
            self.ui_toggle.set_visible(True)
            self.ui_switch.set_visible(False)
            self.ui_toggle.set_active(self.state)
            self.ui_toggle.set_sensitive(self.active)
        else:
            self.ui_toggle.set_visible(False)
            self.ui_switch.set_visible(True)
            self.ui_switch.set_active(self.state)
            self.ui_switch.set_sensitive(self.active)

    def on_toggled(self, button):
        pass

    def on_state_set(self, button, new_state):
        pass

    def build_ui(self):
        root, extra = super().build_ui()
        builder = Gtk.Builder()
        builder.add_from_file(EDIT_BUTTON_UI_RESOURCE_PATH)
        builder.connect_signals(self)
        extra_root = builder.get_object("root")
        extra.add(extra_root)
        self.ui_toggle = builder.get_object("toggle")
        self.ui_switch = builder.get_object("switch")
        self._update_value()
        return root

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        ret = cls(com, ctyp, uid, packet.state, packet.active, packet.button_type)
        return ret


class ControlEditSlider(ControlBase):
    PACKET_CLASS = ControlPacketEditSlider

    def __init__(self, com, ctype, uid, value, active: bool, min_value, max_value, step_size,
                 display_float: bool, display_float_offset, display_float_div):
        super().__init__(com, ctype, uid)
        self.value = value
        self.active = active
        self.min_value = min_value
        self.max_value = max_value
        self.step_size = step_size
        self.display_float = display_float
        self.display_float_offset = display_float_offset
        self.display_float_div = display_float_div

    def get_actual(self, v):
        if self.display_float:
            return (v + self.display_float_offset) / self.display_float_div
        else:
            return v

    def _update_value(self):
        "Must always run inside the mainloop."
        actual_value = self.get_actual(self.value)
        actual_min = self.get_actual(self.min_value)
        actual_max = self.get_actual(self.max_value)
        actual_step = self.get_actual(self.step_size)
        self.ui.set_sensitive(self.active)
        self.ui_data.configure(actual_value, actual_min, actual_max,
                               actual_step, actual_step * 10, 0.0)

    def on_user_input(self, slider):
        pass

    def build_ui(self):
        root, extra = super().build_ui()
        builder = Gtk.Builder()
        builder.add_from_file(EDIT_SLIDER_UI_RESOURCE_PATH)
        builder.connect_signals(self)
        extra_root = builder.get_object("root")
        extra.add(extra_root)
        self.ui = builder.get_object("slider")
        self.ui_data = builder.get_object("slider_data")
        self._update_value()
        return root

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        ret = cls(com, ctyp, uid, packet.value, packet.active != 0, packet.min_value, packet.max_value,
                  packet.step_size, packet.display_float != 0, packet.display_float_offset, packet.display_float_div)
        return ret


class ControlEditColor(ControlBase):
    PACKET_CLASS = ControlPacketEditColor

    def __init__(self, com, ctype, uid, red, green, blue, intensity, active: bool):
        super().__init__(com, ctype, uid)
        self.red = red
        self.green = green
        self.blue = blue
        self.intensity = intensity
        self.active = active

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        ret = cls(com, ctyp, uid, packet.red, packet.green, packet.blue,
                  packet.intensity, packet.active != 0)
        return ret


class ControlEditMultiselect(ControlBase):
    PACKET_CLASS = ControlPacketEditMultiselect

    def __init__(self, com, ctype, uid, selected, active: bool, count):
        super().__init__(com, ctype, uid)
        self.selected = selected
        self.active = active
        self.count = count

    @classmethod
    def construct_from_data(cls, com: Communicator, data: bytes):
        packet = cls.PACKET_CLASS(string=data)
        uid = packet.header.uid
        ctyp = packet.header.type
        ret = cls(com, ctyp, uid, packet.selected, packet.active != 0, packet.count)
        return ret


CONTROL_TYPE_TABLE = {
    1: ControlDisplayString,
    2: ControlDisplayInteger,
    3: ControlDisplayDecimal,
    4: ControlDisplayTime,
    5: ControlDisplayTemp,
    6: ControlEditButton,
    7: ControlEditSlider,
    8: ControlEditColor,
    9: ControlEditMultiselect
}


def parse_update_packet(com: Communicator, data: bytes):
    packet_header = ControlPacket.decode_header(data)
    control_type = ControlType(packet_header.type)
    ret = CONTROL_TYPE_TABLE[control_type].construct_from_data(com, data)
    return ret
