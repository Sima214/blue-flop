.PHONY: size
size: elf
	@$(SIZE) -A -d $(BUILD_DIR)/blueflop.elf

.PHONY: asm
asm: elf
	@$(OBJDUMP) -S $(BUILD_DIR)/blueflop.elf > $(BUILD_DIR)/blueflop.S

.PHONY: bin
bin: elf
	@$(OBJCOPY) -O binary $(BUILD_DIR)/blueflop.elf $(BUILD_DIR)/blueflop.bin

# Install/Flash target.
.PHONY: flash
flash: bin
	stm32flash -w build/blueflop.bin -v $(FTDI_TTY)

.PHONY: run
run: flash
	stm32flash -g 0x0 $(FTDI_TTY)

.PHONY: quickrun
quickrun:
	stm32flash -g 0x0 $(FTDI_TTY)
