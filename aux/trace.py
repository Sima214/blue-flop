import cstruct

class Fault(cstruct.CStruct):
    __byte_order__ = cstruct.LITTLE_ENDIAN
    __struct__ = """
        uint32_t r0;
        uint32_t r1;
        uint32_t r2;
        uint32_t r3;
        uint32_t r12;
        uint32_t lr;
        uint32_t pc;
        uint32_t psr;
        uint32_t bfar;
        uint32_t cfsr;
        uint32_t hfsr;
        uint32_t dfsr;
        uint32_t afsr;
        uint32_t scb_shcsr;
    """