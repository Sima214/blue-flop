#!/usr/bin/env python
import sys
import json
import lzma
import ubjson

ifn = sys.argv[1]
ofn = sys.argv[2]

with open(ifn, 'r') as f, open(ofn, 'w') as o:
    s = f.read()
    j = json.loads(s)
    b = ubjson.dumpb(j)
    ub = len(b)
    print("Uncompressed control data is %u bytes." % ub)
    l = lzma.LZMACompressor(lzma.FORMAT_RAW, filters=[{
                            "id": lzma.FILTER_LZMA2, "preset": 9}])
    b = l.compress(b)
    b = b + l.flush()
    cb = len(b)
    print("Compressed control data is %u bytes." % cb)
    print("Ratio is %i%%" % ((cb / ub) * 100))
    o.write("const char CONTROL_EX_DATA[] = {")
    for i in range(len(b)):
        if i == 0:
            o.write("%i" % b[i])
        else:
            o.write(", %i" % b[i])
    o.write("};")
