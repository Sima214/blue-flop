# blue-flop

This is something that started as a random program to learn the inner workings of arm architecture and the stm32 family of microcontrollers. It uses the [bluepill board](https://wiki.stm32duino.com/index.php?title=Blue_Pill) as a development platform.

Right now it's more like a slave RTOS, controlled by the master program, which capable of syncing to media playback or whatever can update a file. The configuration is stored in the flash of the micro and the master can adapt to the capabilities of the slave.

More info:

- [Resource allocation table](https://drive.google.com/open?id=1rWNMut61xsVaEHWCbWecdQOcKnsGQUUeN6pEt2UtBVQ).
